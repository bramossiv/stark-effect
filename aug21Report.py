#%% modules 
import re
import string

import numpy as np
import pandas as pd
import altair as alt

from matplotlib import cm
from scipy import signal

alt.renderers.enable('mimetype')

#%% init hyperparam
N = 500; ts = 1e-6
amplitude = 5
R = 10e3
C = 10e-6
A = (22 * 4 * 1e-6) * (25 * 1e-6) 
E_0 = 8.854-12

#%% methods for design simulation visualization
def loadSim(fp, scaleX=True):
    """
    Load LTSpice simulation output as long Pandas DataFrame
    """
    # load data
    df = pd.read_csv(fp, sep='\t')
    # clean punctuation from col names
    trans = str.maketrans('', '', string.punctuation)
    df.columns = df.columns.str.translate(trans)
    # load x and optionally scale x-axis values 
    x = df.columns[0]
    if scaleX:
        df[x] = df[x] / ts
    # manually translate wide to long
    N = len(df)
    time = df[x].values.tolist()
    data = {x:[], 'measure':[], 'node':[], 'val':[]}
    for col in df.columns:
        if col == x:
            continue
        measure = col[0] * N
        node = [col[1:]] * N
        val = df[col].values.tolist()

        data[x] += time
        data['measure'] += measure
        data['node'] += node
        data['val'] += val 
    ldf = pd.DataFrame(data=data)
    return ldf 

def plotTransSim(input, layer=False):
    if type(input) == str:
        df = loadSim(input)
    else:
        # assume dataframe input
        df = input.copy()
    
    title = 'Transient Circuit Simulation'
    # split data by measure
    vdf = df[df['measure'] == 'V']
    idf = df[df['measure'] == 'I']
    # domains.....
    vRange = [vdf['val'].min()-10, vdf['val'].max()+10]
    iRange = [idf['val'].min()*2, idf['val'].max()*2]
    # create Altair plots
    x = alt.X('time', 
        axis=alt.Axis(title='Time (microseconds)'
        )
    )
    # vPlot = alt.Chart(vdf, title='Voltage Response Across Nodes').mark_line().encode(
    #     x = x,
    #     y = alt.Y('val:Q', title='Voltage (V)'),
    #     color = 'node'
    # )
    # iPlot = alt.Chart(idf, title='Current Response Across Nodes').mark_line().encode(
    #     x = x,
    #     y = alt.Y('val', title='Current (A)'),
    #     color = 'node'
    # )
    vPlot = alt.Chart(vdf, title=title).mark_line(strokeWidth=3, opacity=.8).encode(
        x = x,
        y = alt.Y('val', 
            axis = alt.Axis(title='Voltage (V)', 
                titleColor='darkblue', labelColor='darkblue',
                grid=True, gridColor='darkblue', gridOpacity=.15
            ),
            scale = alt.Scale(domain=vRange)
        ),
        color = alt.Color('node', title='Voltage Node', scale=alt.Scale(scheme='blues'))
    )
    iPlot = alt.Chart(idf, title=title).mark_line(strokeDash=[20,5], strokeWidth=3, opacity=.8).encode(
        x = x,
        y = alt.Y('val', 
            axis = alt.Axis(title='Current (mA)', 
                titleColor = 'darkred', labelColor='darkred',
                grid=True, gridColor='darkred', gridOpacity=.15, gridDash=[5,5]),
            scale = alt.Scale(domain=iRange)
        ),
        color = alt.Color('node', title='Current Node', scale=alt.Scale(scheme='reds'))
    )
    if layer:
        return alt.layer(vPlot, iPlot).resolve_scale(
            y = 'independent',
            color='independent'
        ).properties(height=400, width=500)
    else:
        return vPlot, iPlot

#%% single jFET sim
fp = r'fet_design\fet_trans_sim.txt'
plotTransSim(fp, True)

#%% hybrid sim
fp = r'hybrid3\hybrid3_trans.txt'
plotTransSim(fp, True)

#%% full bridge sim
fp = r'dual_supply650\full_bridge_sim_trans.txt'
df = loadSim(fp)
df = df[::3].copy()
plotTransSim(df, True)

#%%
fp = r'dual_supply650\single_supply650.txt'
df = loadSim(fp)
df = df[::3].copy()
plotTransSim(df, True)
#%%
fp = r'PA96\pa96_transsim.txt'
df = loadSim(fp)
df = df[::5]
plotTransSim(df, True)
#%%
v
#%%
i
#%% single JFET AC analysis
fp = r'fet_design\fet_acsim.txt'
df = loadSim(fp, False)
df['val'] = [float(x.split(',')[0][1:-2]) for x in df['val']]

title = 'Frequency Response Simulation'
vdf = df[df['measure'] == 'V']
idf = df[df['measure'] == 'I']

x = alt.X('Freq', 
    axis=alt.Axis(title='Frequency (Hz)'),
    scale=alt.Scale(type='log')) 
y = alt.Y('val', title='Gain (dB)')
vPlot = alt.Chart(vdf, title=title).mark_line().encode(
    x = x,
    y = y,
    color = alt.Color('node', title='Voltage Node', scale=alt.Scale(scheme='blues'))
)
iPlot = alt.Chart(idf, title=title).mark_line().encode(
    x = x,
    y = y,
    color = alt.Color('node', title='Current Node', scale=alt.Scale(scheme='reds'))
)
alt.layer(vPlot, iPlot).resolve_scale(
    color='independent'
).properties(height=300, width=300)

#%% single JFET DC analysis
fp = r'fet_design\fet_dcsim.txt'
df = loadSim(fp, False)

title = 'Power Supply (V1) DC Sweep Simulation'
vdf = df[df['measure'] == 'V']
idf = df[df['measure'] == 'I']
# scale currents
idf['val'] = idf['val'] * -1000

x = alt.X('v1', 
    axis=alt.Axis(title='Voltage Supply'),
    scale = alt.Scale(domain=[10, 200], zero=False)) 
vPlot = alt.Chart(vdf, title=title).mark_line(strokeWidth=3).encode(
    x = x,
    y = alt.Y('val', 
        axis = alt.Axis(title='Voltage (V)', 
            titleColor='darkblue', labelColor='darkblue',
            grid=True, gridColor='darkblue', gridOpacity=.15),
        scale = alt.Scale(domain=[-55, 205])
    ),
    color = alt.Color('node', title='Voltage Node', scale=alt.Scale(scheme='blues'))
)
iPlot = alt.Chart(idf, title=title).mark_line(strokeDash=[20,5], strokeWidth=3).encode(
    x = x,
    y = alt.Y('val', 
        axis = alt.Axis(title='Current (mA)', 
            titleColor = 'darkred', labelColor='darkred',
            grid=True, gridColor='darkred', gridOpacity=.15, gridDash=[5,5]),
        scale=alt.Scale(domain=[0, 2.5])
    ),
    color = alt.Color('node', title='Current Node', scale=alt.Scale(scheme='reds'))
)
alt.layer(vPlot, iPlot).resolve_scale(
    y = 'independent',
    color='independent'
).properties(height=300, width=300)

#%% old clockgen
n = 2
fp = r'data' + f'\{n}mhzinputclockgen.csv'
df = pd.read_csv(fp, skiprows=12, names=['time', 'v'])
df['time'] *= 1e6

alt.Chart(df, 
    title = f'Clock Generator Output at {n}MHz'
).mark_line(
).encode(
        x = alt.X('time', title='Time (microseconds)'),
        y = alt.Y('v', title='Voltage (V)')
)

#%%
#cm2hex = lambda t: '#%02x%02x%02x' % tuple((t[:3]*255).astype(int))
#vColors = [cm2hex(c) for c in cm.Set1(np.arange(len(vCols)))]
#iColors = [cm2hex(c) for c in cm.Set2(np.arange(len(iCols)))]

#%%
vPlot
