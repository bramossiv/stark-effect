#%% modules
import numpy as np

#%% methods
def db2me(db):
    return np.power(db, db/10)
def me2db(me):
    return 10*np.log10([me])[0]

# %%
print(db2me(1))
print(me2db(20/95))

# %%
