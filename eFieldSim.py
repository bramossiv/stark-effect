#%% modules 
import numpy as np
import pandas as pd
import altair as alt

from scipy import signal

alt.renderers.enable('mimetype')

#%% init variables and calculate
"""
Q(t) = CV(1-exp(-t/RC))
E(t) = e_z * Q(t)/(E_0*A)
"""
N = 500; ts = 1e-6
amplitude = 5
R = 10e3
C = 10e-6
A = (22 * 4 * 1e-6) * (25 * 1e-6) 
E_0 = 8.854-12
timeSignal = np.arange(N) * ts
# inSignal = 10 * np.sin(np.linspace(0, 8 * np.pi, N)) # 4 periods 3V pk-pk
inSignal = amplitude * signal.square(np.linspace(0, N//2 * np.pi, N)) # 4 periods 3V pk-pk
Q = inSignal * C * (1 - np.exp(-timeSignal / (R*C)))
E = Q / ( E_0 * A)

# %% init data
data = {'in':inSignal, 'time':timeSignal, 'Q':Q, 'E':E}
df = pd.DataFrame(data=data)
# %%
df['time'] = df['time'] / ts

eColor = '#0047AB'
vColor = '#CD7F32'
opacity = .6

x = alt.X('time', 
    axis=alt.Axis(title='Time (microseconds)'
    )
)
y = alt.Y('E', 
    axis = alt.Axis(title='Electric Field (V/m)',
        titleColor = eColor
    )
)
y2 = alt.Y('in', 
    axis = alt.Axis(title='Voltage applied',
        titleColor = vColor
    ),
    scale = alt.Scale(domain = [-amplitude * 2, amplitude * 2])
)

EPlot = alt.Chart(df).mark_line(color = eColor, opacity = opacity).encode(
    x = x,
    y = y
)
VPlot = alt.Chart(df).mark_line(color = vColor, opacity = opacity).encode(
    x = x,
    y = y2
)
alt.layer(VPlot, EPlot).resolve_scale(
    y = 'independent'
)
# %%
5//3

# %%
