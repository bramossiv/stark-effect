EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Bias Design"
Date "2021-09-16"
Rev "A"
Comp "Sivananthan Laboratories"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR0101
U 1 1 6145E172
P 7850 6300
F 0 "#PWR0101" H 7850 6050 50  0001 C CNN
F 1 "GND" H 7855 6127 50  0000 C CNN
F 2 "" H 7850 6300 50  0001 C CNN
F 3 "" H 7850 6300 50  0001 C CNN
	1    7850 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 6145E7C3
P 7850 4400
F 0 "#PWR0102" H 7850 4150 50  0001 C CNN
F 1 "GND" H 7855 4227 50  0000 C CNN
F 2 "" H 7850 4400 50  0001 C CNN
F 3 "" H 7850 4400 50  0001 C CNN
	1    7850 4400
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IPP060N06N Q5
U 1 1 614881B0
P 7750 4100
F 0 "Q5" H 7955 4146 50  0000 L CNN
F 1 "IPP060N06N" H 7955 4055 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 8000 4025 50  0001 L CIN
F 3 "https://www.infineon.com/dgdl/Infineon-IPP060N06N-DS-v02_02-en.pdf?fileId=db3a30433727a44301372c06d9d7498a" H 7750 4100 50  0001 L CNN
	1    7750 4100
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IPP060N06N Q6
U 1 1 6148C649
P 7750 6000
F 0 "Q6" H 7955 6046 50  0000 L CNN
F 1 "IPP060N06N" H 7955 5955 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 8000 5925 50  0001 L CIN
F 3 "https://www.infineon.com/dgdl/Infineon-IPP060N06N-DS-v02_02-en.pdf?fileId=db3a30433727a44301372c06d9d7498a" H 7750 6000 50  0001 L CNN
	1    7750 6000
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:TEH140M50R0FE R8
U 1 1 61492589
P 7850 3100
F 0 "R8" H 8200 3325 50  0000 C CNN
F 1 "TEH140M50R0FE" H 8200 3234 50  0000 C CNN
F 2 "TEH140M50R0FE" H 8400 3150 50  0001 L CNN
F 3 "https://www.mouser.com/datasheet/2/303/res_teh140-1488069.pdf" H 8400 3050 50  0001 L CNN
F 4 "RES, 50R, 140W, TO-247, THICK FILM; Resistance:50ohm; Product Range:TEH140 Series; Power Rating:140W; Resistance Tolerance:+/- 1%; Resistor Case Style:TO-247; Voltage Rating:500V; Resistor Element Type:Thick Film RoHS Compliant: Yes" H 8400 2950 50  0001 L CNN "Description"
F 5 "20.5" H 8400 2850 50  0001 L CNN "Height"
F 6 "Ohmite" H 8400 2750 50  0001 L CNN "Manufacturer_Name"
F 7 "TEH140M50R0FE" H 8400 2650 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "588-TEH140M50R0FE" H 8400 2550 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=588-TEH140M50R0FE" H 8400 2450 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 8400 2350 50  0001 L CNN "Arrow Part Number"
F 11 "" H 8400 2250 50  0001 L CNN "Arrow Price/Stock"
	1    7850 3100
	0    1    1    0   
$EndComp
$Comp
L SamacSys_Parts:TEH140M50R0FE R7
U 1 1 61498694
P 7850 2300
F 0 "R7" H 8200 2525 50  0000 C CNN
F 1 "TEH140M50R0FE" H 8200 2434 50  0000 C CNN
F 2 "TEH140M50R0FE" H 8400 2350 50  0001 L CNN
F 3 "https://www.mouser.com/datasheet/2/303/res_teh140-1488069.pdf" H 8400 2250 50  0001 L CNN
F 4 "RES, 50R, 140W, TO-247, THICK FILM; Resistance:50ohm; Product Range:TEH140 Series; Power Rating:140W; Resistance Tolerance:+/- 1%; Resistor Case Style:TO-247; Voltage Rating:500V; Resistor Element Type:Thick Film RoHS Compliant: Yes" H 8400 2150 50  0001 L CNN "Description"
F 5 "20.5" H 8400 2050 50  0001 L CNN "Height"
F 6 "Ohmite" H 8400 1950 50  0001 L CNN "Manufacturer_Name"
F 7 "TEH140M50R0FE" H 8400 1850 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "588-TEH140M50R0FE" H 8400 1750 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=588-TEH140M50R0FE" H 8400 1650 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 8400 1550 50  0001 L CNN "Arrow Part Number"
F 11 "" H 8400 1450 50  0001 L CNN "Arrow Price/Stock"
	1    7850 2300
	0    1    1    0   
$EndComp
$Comp
L SamacSys_Parts:TEH140M50R0FE R6
U 1 1 6149A50B
P 7850 1500
F 0 "R6" H 8200 1725 50  0000 C CNN
F 1 "TEH140M50R0FE" H 8200 1634 50  0000 C CNN
F 2 "TEH140M50R0FE" H 8400 1550 50  0001 L CNN
F 3 "https://www.mouser.com/datasheet/2/303/res_teh140-1488069.pdf" H 8400 1450 50  0001 L CNN
F 4 "RES, 50R, 140W, TO-247, THICK FILM; Resistance:50ohm; Product Range:TEH140 Series; Power Rating:140W; Resistance Tolerance:+/- 1%; Resistor Case Style:TO-247; Voltage Rating:500V; Resistor Element Type:Thick Film RoHS Compliant: Yes" H 8400 1350 50  0001 L CNN "Description"
F 5 "20.5" H 8400 1250 50  0001 L CNN "Height"
F 6 "Ohmite" H 8400 1150 50  0001 L CNN "Manufacturer_Name"
F 7 "TEH140M50R0FE" H 8400 1050 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "588-TEH140M50R0FE" H 8400 950 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=588-TEH140M50R0FE" H 8400 850 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 8400 750 50  0001 L CNN "Arrow Part Number"
F 11 "" H 8400 650 50  0001 L CNN "Arrow Price/Stock"
	1    7850 1500
	0    1    1    0   
$EndComp
$Comp
L SamacSys_Parts:LM5134AMF_NOPB IC3
U 1 1 6149F937
P 5800 4200
F 0 "IC3" H 6350 4465 50  0000 C CNN
F 1 "LM5134AMF_NOPB" H 6350 4374 50  0000 C CNN
F 2 "SOT95P280X145-6N" H 6750 4300 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/lm5134" H 6750 4200 50  0001 L CNN
F 4 "Single, 7.6A Peak Current Low-Side Gate Driver with Alternative Pilot Output" H 6750 4100 50  0001 L CNN "Description"
F 5 "1.45" H 6750 4000 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 6750 3900 50  0001 L CNN "Manufacturer_Name"
F 7 "LM5134AMF/NOPB" H 6750 3800 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 6750 3700 50  0001 L CNN "Mouser Part Number"
F 9 "" H 6750 3600 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 6750 3500 50  0001 L CNN "Arrow Part Number"
F 11 "" H 6750 3400 50  0001 L CNN "Arrow Price/Stock"
	1    5800 4200
	-1   0    0    1   
$EndComp
$Comp
L SamacSys_Parts:LM5134AMF_NOPB IC2
U 1 1 614B63AC
P 5750 6100
F 0 "IC2" H 6300 5635 50  0000 C CNN
F 1 "LM5134AMF_NOPB" H 6300 5726 50  0000 C CNN
F 2 "SOT95P280X145-6N" H 6700 6200 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/lm5134" H 6700 6100 50  0001 L CNN
F 4 "Single, 7.6A Peak Current Low-Side Gate Driver with Alternative Pilot Output" H 6700 6000 50  0001 L CNN "Description"
F 5 "1.45" H 6700 5900 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 6700 5800 50  0001 L CNN "Manufacturer_Name"
F 7 "LM5134AMF/NOPB" H 6700 5700 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 6700 5600 50  0001 L CNN "Mouser Part Number"
F 9 "" H 6700 5500 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 6700 5400 50  0001 L CNN "Arrow Part Number"
F 11 "" H 6700 5300 50  0001 L CNN "Arrow Price/Stock"
	1    5750 6100
	-1   0    0    1   
$EndComp
$Comp
L SamacSys_Parts:CHP0805AJW-100ELF R3
U 1 1 614C4988
P 6750 4100
F 0 "R3" H 7100 4325 50  0000 C CNN
F 1 "CHP0805AJW-100ELF" H 7100 4234 50  0000 C CNN
F 2 "RESC2012X60N" H 7300 4150 50  0001 L CNN
F 3 "https://www.mouser.com/datasheet/2/54/chp-a-1858677.pdf" H 7300 4050 50  0001 L CNN
F 4 "Thick Film Resistors - SMD ResHighPowerA 0805 10R 5% 1/2W TC200 AEC-Q200" H 7300 3950 50  0001 L CNN "Description"
F 5 "0.6" H 7300 3850 50  0001 L CNN "Height"
F 6 "Bourns" H 7300 3750 50  0001 L CNN "Manufacturer_Name"
F 7 "CHP0805AJW-100ELF" H 7300 3650 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "652-CHP0805AJW-100L" H 7300 3550 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Bourns/CHP0805AJW-100ELF?qs=GedFDFLaBXFSSMiH%252B2UbRQ%3D%3D" H 7300 3450 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 7300 3350 50  0001 L CNN "Arrow Part Number"
F 11 "" H 7300 3250 50  0001 L CNN "Arrow Price/Stock"
	1    6750 4100
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:CHP0805AJW-100ELF R4
U 1 1 614C6BBB
P 6750 6000
F 0 "R4" H 7100 6225 50  0000 C CNN
F 1 "CHP0805AJW-100ELF" H 7100 6134 50  0000 C CNN
F 2 "RESC2012X60N" H 7300 6050 50  0001 L CNN
F 3 "https://www.mouser.com/datasheet/2/54/chp-a-1858677.pdf" H 7300 5950 50  0001 L CNN
F 4 "Thick Film Resistors - SMD ResHighPowerA 0805 10R 5% 1/2W TC200 AEC-Q200" H 7300 5850 50  0001 L CNN "Description"
F 5 "0.6" H 7300 5750 50  0001 L CNN "Height"
F 6 "Bourns" H 7300 5650 50  0001 L CNN "Manufacturer_Name"
F 7 "CHP0805AJW-100ELF" H 7300 5550 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "652-CHP0805AJW-100L" H 7300 5450 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Bourns/CHP0805AJW-100ELF?qs=GedFDFLaBXFSSMiH%252B2UbRQ%3D%3D" H 7300 5350 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 7300 5250 50  0001 L CNN "Arrow Part Number"
F 11 "" H 7300 5150 50  0001 L CNN "Arrow Price/Stock"
	1    6750 6000
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:2N4401_PBFREE Q1
U 1 1 614C7D81
P 6200 3700
F 0 "Q1" H 6738 3746 50  0000 L CNN
F 1 "2N4401_PBFREE" H 6738 3655 50  0000 L CNN
F 2 "2N4401PBFREE" H 6750 3550 50  0001 L CNN
F 3 "https://my.centralsemi.com/datasheets/2N4400.PDF" H 6750 3450 50  0001 L CNN
F 4 "Bipolar Transistors - BJT NPN Gen Pur SS" H 6750 3350 50  0001 L CNN "Description"
F 5 "5.33" H 6750 3250 50  0001 L CNN "Height"
F 6 "Central Semiconductor" H 6750 3150 50  0001 L CNN "Manufacturer_Name"
F 7 "2N4401 PBFREE" H 6750 3050 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "610-2N4401" H 6750 2950 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Central-Semiconductor/2N4401-PBFREE?qs=u16ybLDytRbENLVWEhN70w%3D%3D" H 6750 2850 50  0001 L CNN "Mouser Price/Stock"
F 10 "2N4401 PBFREE" H 6750 2750 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/2n4401-pbfree/central-semiconductor?region=nac" H 6750 2650 50  0001 L CNN "Arrow Price/Stock"
	1    6200 3700
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:2N4401_PBFREE Q3
U 1 1 614CD98F
P 6200 5600
F 0 "Q3" H 6738 5646 50  0000 L CNN
F 1 "2N4401_PBFREE" H 6738 5555 50  0000 L CNN
F 2 "2N4401PBFREE" H 6750 5450 50  0001 L CNN
F 3 "https://my.centralsemi.com/datasheets/2N4400.PDF" H 6750 5350 50  0001 L CNN
F 4 "Bipolar Transistors - BJT NPN Gen Pur SS" H 6750 5250 50  0001 L CNN "Description"
F 5 "5.33" H 6750 5150 50  0001 L CNN "Height"
F 6 "Central Semiconductor" H 6750 5050 50  0001 L CNN "Manufacturer_Name"
F 7 "2N4401 PBFREE" H 6750 4950 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "610-2N4401" H 6750 4850 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Central-Semiconductor/2N4401-PBFREE?qs=u16ybLDytRbENLVWEhN70w%3D%3D" H 6750 4750 50  0001 L CNN "Mouser Price/Stock"
F 10 "2N4401 PBFREE" H 6750 4650 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/2n4401-pbfree/central-semiconductor?region=nac" H 6750 4550 50  0001 L CNN "Arrow Price/Stock"
	1    6200 5600
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:2N4403 Q2
U 1 1 614EB587
P 6200 4500
F 0 "Q2" H 6738 4546 50  0000 L CNN
F 1 "2N4403" H 6738 4455 50  0000 L CNN
F 2 "CS_TO-92" H 6750 4350 50  0001 L CNN
F 3 "https://www.mouser.com/ds/2/68/2n4402-46341.pdf" H 6750 4250 50  0001 L CNN
F 4 "Bipolar Transistors - BJT PNP Gen Pur SS" H 6750 4150 50  0001 L CNN "Description"
F 5 "" H 6750 4050 50  0001 L CNN "Height"
F 6 "Central Semiconductor" H 6750 3950 50  0001 L CNN "Manufacturer_Name"
F 7 "2N4403" H 6750 3850 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "610-2N4403" H 6750 3750 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=610-2N4403" H 6750 3650 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 6750 3550 50  0001 L CNN "Arrow Part Number"
F 11 "" H 6750 3450 50  0001 L CNN "Arrow Price/Stock"
	1    6200 4500
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:2N4403 Q4
U 1 1 614ED601
P 6200 6400
F 0 "Q4" H 6738 6446 50  0000 L CNN
F 1 "2N4403" H 6738 6355 50  0000 L CNN
F 2 "CS_TO-92" H 6750 6250 50  0001 L CNN
F 3 "https://www.mouser.com/ds/2/68/2n4402-46341.pdf" H 6750 6150 50  0001 L CNN
F 4 "Bipolar Transistors - BJT PNP Gen Pur SS" H 6750 6050 50  0001 L CNN "Description"
F 5 "" H 6750 5950 50  0001 L CNN "Height"
F 6 "Central Semiconductor" H 6750 5850 50  0001 L CNN "Manufacturer_Name"
F 7 "2N4403" H 6750 5750 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "610-2N4403" H 6750 5650 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=610-2N4403" H 6750 5550 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 6750 5450 50  0001 L CNN "Arrow Part Number"
F 11 "" H 6750 5350 50  0001 L CNN "Arrow Price/Stock"
	1    6200 6400
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:284414-2 J5
U 1 1 614FC6E9
P 9650 4800
F 0 "J5" H 10278 4796 50  0000 L CNN
F 1 "284414-2" H 10278 4705 50  0000 L CNN
F 2 "2844142" H 10300 4900 50  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=284414&DocType=Customer+Drawing&DocLang=English" H 10300 4800 50  0001 L CNN
F 4 "Fixed Terminal Blocks TERMI-BLOK 900 PCB 2 POS. 3,5" H 10300 4700 50  0001 L CNN "Description"
F 5 "8.35" H 10300 4600 50  0001 L CNN "Height"
F 6 "TE Connectivity" H 10300 4500 50  0001 L CNN "Manufacturer_Name"
F 7 "284414-2" H 10300 4400 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "571-284414-2" H 10300 4300 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/TE-Connectivity/284414-2?qs=pW%2FyRk%2FT1EGY%252BVf9FdE6RA%3D%3D" H 10300 4200 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 10300 4100 50  0001 L CNN "Arrow Part Number"
F 11 "" H 10300 4000 50  0001 L CNN "Arrow Price/Stock"
	1    9650 4800
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:284414-2 J1
U 1 1 61502F90
P 1450 6850
F 0 "J1" V 1696 6978 50  0000 L CNN
F 1 "284414-2" V 1787 6978 50  0000 L CNN
F 2 "2844142" H 2100 6950 50  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=284414&DocType=Customer+Drawing&DocLang=English" H 2100 6850 50  0001 L CNN
F 4 "Fixed Terminal Blocks TERMI-BLOK 900 PCB 2 POS. 3,5" H 2100 6750 50  0001 L CNN "Description"
F 5 "8.35" H 2100 6650 50  0001 L CNN "Height"
F 6 "TE Connectivity" H 2100 6550 50  0001 L CNN "Manufacturer_Name"
F 7 "284414-2" H 2100 6450 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "571-284414-2" H 2100 6350 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/TE-Connectivity/284414-2?qs=pW%2FyRk%2FT1EGY%252BVf9FdE6RA%3D%3D" H 2100 6250 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 2100 6150 50  0001 L CNN "Arrow Part Number"
F 11 "" H 2100 6050 50  0001 L CNN "Arrow Price/Stock"
	1    1450 6850
	0    -1   -1   0   
$EndComp
$Comp
L SamacSys_Parts:CRCW04021K00FKEDC R1
U 1 1 61514FFF
P 2800 3500
F 0 "R1" H 3150 3725 50  0000 C CNN
F 1 "CRCW04021K00FKEDC" H 3150 3634 50  0000 C CNN
F 2 "RESC1005X35N" H 3350 3550 50  0001 L CNN
F 3 "https://www.mouser.com/datasheet/2/427/crcwce3-1762584.pdf" H 3350 3450 50  0001 L CNN
F 4 "D10/CRCW0402-C 100 1K0 1% ET7" H 3350 3350 50  0001 L CNN "Description"
F 5 "0.35" H 3350 3250 50  0001 L CNN "Height"
F 6 "Vishay" H 3350 3150 50  0001 L CNN "Manufacturer_Name"
F 7 "CRCW04021K00FKEDC" H 3350 3050 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "71-CRCW04021K00FKEDC" H 3350 2950 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Vishay-Dale/CRCW04021K00FKEDC?qs=E3Y5ESvWgWMj5mY%252BhDIyqg%3D%3D" H 3350 2850 50  0001 L CNN "Mouser Price/Stock"
F 10 "CRCW04021K00FKEDC" H 3350 2750 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/crcw04021k00fkedc/vishay?region=nac" H 3350 2650 50  0001 L CNN "Arrow Price/Stock"
	1    2800 3500
	0    -1   -1   0   
$EndComp
$Comp
L SamacSys_Parts:CRCW04021K00FKEDC R2
U 1 1 6151BFFF
P 3150 3500
F 0 "R2" H 3500 3725 50  0000 C CNN
F 1 "CRCW04021K00FKEDC" H 3500 3634 50  0000 C CNN
F 2 "RESC1005X35N" H 3700 3550 50  0001 L CNN
F 3 "https://www.mouser.com/datasheet/2/427/crcwce3-1762584.pdf" H 3700 3450 50  0001 L CNN
F 4 "D10/CRCW0402-C 100 1K0 1% ET7" H 3700 3350 50  0001 L CNN "Description"
F 5 "0.35" H 3700 3250 50  0001 L CNN "Height"
F 6 "Vishay" H 3700 3150 50  0001 L CNN "Manufacturer_Name"
F 7 "CRCW04021K00FKEDC" H 3700 3050 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "71-CRCW04021K00FKEDC" H 3700 2950 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Vishay-Dale/CRCW04021K00FKEDC?qs=E3Y5ESvWgWMj5mY%252BhDIyqg%3D%3D" H 3700 2850 50  0001 L CNN "Mouser Price/Stock"
F 10 "CRCW04021K00FKEDC" H 3700 2750 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/crcw04021k00fkedc/vishay?region=nac" H 3700 2650 50  0001 L CNN "Arrow Price/Stock"
	1    3150 3500
	0    -1   -1   0   
$EndComp
$Comp
L SamacSys_Parts:284414-2 J4
U 1 1 61501515
P 6950 850
F 0 "J4" V 7288 622 50  0000 R CNN
F 1 "284414-2" V 7197 622 50  0000 R CNN
F 2 "2844142" H 7600 950 50  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=284414&DocType=Customer+Drawing&DocLang=English" H 7600 850 50  0001 L CNN
F 4 "Fixed Terminal Blocks TERMI-BLOK 900 PCB 2 POS. 3,5" H 7600 750 50  0001 L CNN "Description"
F 5 "8.35" H 7600 650 50  0001 L CNN "Height"
F 6 "TE Connectivity" H 7600 550 50  0001 L CNN "Manufacturer_Name"
F 7 "284414-2" H 7600 450 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "571-284414-2" H 7600 350 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/TE-Connectivity/284414-2?qs=pW%2FyRk%2FT1EGY%252BVf9FdE6RA%3D%3D" H 7600 250 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 7600 150 50  0001 L CNN "Arrow Part Number"
F 11 "" H 7600 50  50  0001 L CNN "Arrow Price/Stock"
	1    6950 850 
	0    1    1    0   
$EndComp
$Comp
L MCU_Module:Arduino_Nano_v3.x A1
U 1 1 6150ED26
P 1450 2200
F 0 "A1" H 1450 1111 50  0000 C CNN
F 1 "Arduino_Nano_v3.x" H 1450 1020 50  0000 C CNN
F 2 "Module:Arduino_Nano" H 1450 2200 50  0001 C CIN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 1450 2200 50  0001 C CNN
	1    1450 2200
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:FA-238_25.0000MB-C3 Y1
U 1 1 61552E48
P 2200 1950
F 0 "Y1" H 3000 2215 50  0000 C CNN
F 1 "FA-238_25.0000MB-C3" H 3000 2124 50  0000 C CNN
F 2 "FA238300000MBC0" H 3650 2050 50  0001 L CNN
F 3 "https://support.epson.biz/td/api/doc_check.php?dl=brief_FA-238&lang=en" H 3650 1950 50  0001 L CNN
F 4 "Crystals 25MHz 50ppm 18pF -20C +70C" H 3650 1850 50  0001 L CNN "Description"
F 5 "0.7" H 3650 1750 50  0001 L CNN "Height"
F 6 "Epson Timing" H 3650 1650 50  0001 L CNN "Manufacturer_Name"
F 7 "FA-238 25.0000MB-C3" H 3650 1550 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "732-FA238-25B-C3" H 3650 1450 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Epson-Timing/FA-238-250000MB-C3?qs=CANAagUtPAqYIDF5A41Crg%3D%3D" H 3650 1350 50  0001 L CNN "Mouser Price/Stock"
F 10 "FA-238 25.0000MB-C3" H 3650 1250 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/fa-23825.0000mb-c3/epson-electronics-america" H 3650 1150 50  0001 L CNN "Arrow Price/Stock"
	1    2200 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 1400 7850 1500
Wire Wire Line
	7850 3800 7850 3850
Wire Wire Line
	7850 3000 7850 3100
Wire Wire Line
	7850 2200 7850 2300
Wire Wire Line
	7850 4300 7850 4400
Wire Wire Line
	7850 3850 8550 3850
Connection ~ 7850 3850
Wire Wire Line
	7850 3850 7850 3900
$Comp
L power:GND #PWR0103
U 1 1 6156ABCC
P 6600 6800
F 0 "#PWR0103" H 6600 6550 50  0001 C CNN
F 1 "GND" H 6605 6627 50  0000 C CNN
F 2 "" H 6600 6800 50  0001 C CNN
F 3 "" H 6600 6800 50  0001 C CNN
	1    6600 6800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 6156B8FF
P 6600 4900
F 0 "#PWR0104" H 6600 4650 50  0001 C CNN
F 1 "GND" H 6605 4727 50  0000 C CNN
F 2 "" H 6600 4900 50  0001 C CNN
F 3 "" H 6600 4900 50  0001 C CNN
	1    6600 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 600  7850 700 
$Comp
L power:GND #PWR0105
U 1 1 6157DF59
P 6850 650
F 0 "#PWR0105" H 6850 400 50  0001 C CNN
F 1 "GND" H 6855 477 50  0000 C CNN
F 2 "" H 6850 650 50  0001 C CNN
F 3 "" H 6850 650 50  0001 C CNN
	1    6850 650 
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 6157EEBC
P 1450 7000
F 0 "#PWR0106" H 1450 6750 50  0001 C CNN
F 1 "GND" H 1455 6827 50  0000 C CNN
F 2 "" H 1450 7000 50  0001 C CNN
F 3 "" H 1450 7000 50  0001 C CNN
	1    1450 7000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 61580106
P 3000 7050
F 0 "#PWR0107" H 3000 6800 50  0001 C CNN
F 1 "GND" H 3005 6877 50  0000 C CNN
F 2 "" H 3000 7050 50  0001 C CNN
F 3 "" H 3000 7050 50  0001 C CNN
	1    3000 7050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 615973FC
P 2100 3350
F 0 "#PWR0108" H 2100 3100 50  0001 C CNN
F 1 "GND" H 2105 3177 50  0000 C CNN
F 2 "" H 2100 3350 50  0001 C CNN
F 3 "" H 2100 3350 50  0001 C CNN
	1    2100 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 3200 1550 3200
Wire Wire Line
	2100 3200 2100 3350
Connection ~ 1550 3200
Wire Wire Line
	1550 3200 2100 3200
$Comp
L SamacSys_Parts:OPA810IDBVT IC1
U 1 1 615D1F8D
P 4200 5300
F 0 "IC1" H 4750 5565 50  0000 C CNN
F 1 "OPA810IDBVT" H 4750 5474 50  0000 C CNN
F 2 "SOT95P280X145-5N" H 5150 5400 50  0001 L CNN
F 3 "https://www.ti.com/lit/ds/symlink/opa810.pdf?ts=1623319461078&ref_url=https%253A%252F%252Fwww.ti.com%252Fstore%252Fti%252Fen%252Fp%252Fproduct%252F%253Fp%253DOPA810IDBVT" H 5150 5300 50  0001 L CNN
F 4 "Operational Amplifiers - Op Amps Single Channel, High Performance, 27 V, 140 MHz, RRIO FET Input Op Amp 5-SOT-23 -40 to 125" H 5150 5200 50  0001 L CNN "Description"
F 5 "1.45" H 5150 5100 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 5150 5000 50  0001 L CNN "Manufacturer_Name"
F 7 "OPA810IDBVT" H 5150 4900 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "595-OPA810IDBVT" H 5150 4800 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Texas-Instruments/OPA810IDBVT?qs=xZ%2FP%252Ba9zWqaCn5bfRsQX9Q%3D%3D" H 5150 4700 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 5150 4600 50  0001 L CNN "Arrow Part Number"
F 11 "" H 5150 4500 50  0001 L CNN "Arrow Price/Stock"
	1    4200 5300
	-1   0    0    1   
$EndComp
Wire Wire Line
	7850 6300 7850 6200
Wire Wire Line
	7550 6000 7450 6000
Wire Wire Line
	6600 5900 6600 6000
Wire Wire Line
	6750 6000 6600 6000
Connection ~ 6600 6000
Wire Wire Line
	6600 6000 6600 6100
Wire Wire Line
	6600 4800 6600 4900
Wire Wire Line
	7550 4100 7450 4100
Wire Wire Line
	6750 4100 6600 4100
Wire Wire Line
	6600 4100 6600 4000
Wire Wire Line
	6600 4200 6600 4100
Connection ~ 6600 4100
Wire Wire Line
	1550 6850 1550 6950
Text GLabel 6600 5300 1    50   Input ~ 0
10V
Text GLabel 6600 3400 1    50   Input ~ 0
10V
$Comp
L power:GND #PWR0109
U 1 1 615DE5A1
P 4500 4700
F 0 "#PWR0109" H 4500 4450 50  0001 C CNN
F 1 "GND" H 4505 4527 50  0000 C CNN
F 2 "" H 4500 4700 50  0001 C CNN
F 3 "" H 4500 4700 50  0001 C CNN
	1    4500 4700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 615DF19F
P 4550 6400
F 0 "#PWR0110" H 4550 6150 50  0001 C CNN
F 1 "GND" H 4555 6227 50  0000 C CNN
F 2 "" H 4550 6400 50  0001 C CNN
F 3 "" H 4550 6400 50  0001 C CNN
	1    4550 6400
	1    0    0    -1  
$EndComp
Text GLabel 5750 6100 2    50   Input ~ 0
10V
Text GLabel 5250 4700 3    50   Input ~ 0
10V
Wire Wire Line
	5800 4200 5900 4200
Wire Wire Line
	5900 4200 5900 4550
Wire Wire Line
	5900 4550 5250 4550
Wire Wire Line
	4600 4550 4600 4100
Wire Wire Line
	4600 4100 4700 4100
Wire Wire Line
	5250 4550 5250 4700
Connection ~ 5250 4550
Wire Wire Line
	5250 4550 4600 4550
Wire Wire Line
	4650 5900 4550 5900
Wire Wire Line
	4550 5900 4550 6100
Wire Wire Line
	4650 6100 4550 6100
Connection ~ 4550 6100
Wire Wire Line
	4550 6100 4550 6400
Wire Wire Line
	4700 4000 4500 4000
Wire Wire Line
	4500 4000 4500 4700
Wire Wire Line
	5750 5900 6000 5900
Wire Wire Line
	6000 5900 6000 5600
Wire Wire Line
	6000 5600 6200 5600
Wire Wire Line
	6000 5900 6000 6400
Wire Wire Line
	6000 6400 6200 6400
Connection ~ 6000 5900
Wire Wire Line
	5800 4000 6000 4000
Wire Wire Line
	6000 4000 6000 3700
Wire Wire Line
	6000 3700 6200 3700
Wire Wire Line
	6000 4000 6000 4500
Wire Wire Line
	6000 4500 6200 4500
Connection ~ 6000 4000
Wire Wire Line
	6600 6700 6600 6800
Wire Wire Line
	4400 5300 4400 4200
Wire Wire Line
	4400 4200 4700 4200
Wire Wire Line
	4400 5300 4400 6000
Wire Wire Line
	4400 6000 4650 6000
Connection ~ 4400 5300
Wire Wire Line
	3000 6950 3000 7050
Wire Wire Line
	3100 5300 3000 5300
Wire Wire Line
	4200 5300 4400 5300
$Comp
L SamacSys_Parts:RS73F1JTTD2001B R2000
U 1 1 6152B837
P 3000 6250
F 0 "R2000" H 3350 6475 50  0000 C CNN
F 1 "RS73F1JTTD2001B" H 3350 6384 50  0000 C CNN
F 2 "RESC1608X55N" H 3550 6300 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/rs73f1jttd2001b/koa-speer-electronics" H 3550 6200 50  0001 L CNN
F 4 "Res Thick Film 0603 2K Ohm 0.1% 0.2W(1/5W) +/-25ppm/C Pad SMD Automotive T/R" H 3550 6100 50  0001 L CNN "Description"
F 5 "0.55" H 3550 6000 50  0001 L CNN "Height"
F 6 "KOA Speer" H 3550 5900 50  0001 L CNN "Manufacturer_Name"
F 7 "RS73F1JTTD2001B" H 3550 5800 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "660-RS73F1JTTD2001B" H 3550 5700 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/KOA-Speer/RS73F1JTTD2001B?qs=GBLSl2AkirvT9QL%252B%2FsjYag%3D%3D" H 3550 5600 50  0001 L CNN "Mouser Price/Stock"
F 10 "RS73F1JTTD2001B" H 3550 5500 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/rs73f1jttd2001b/koa-speer-electronics" H 3550 5400 50  0001 L CNN "Arrow Price/Stock"
	1    3000 6250
	0    1    1    0   
$EndComp
$Comp
L SamacSys_Parts:CMP1206AFX-5601ELF R5600
U 1 1 6151EC9D
P 3300 6000
F 0 "R5600" H 3650 6225 50  0000 C CNN
F 1 "CMP1206AFX-5601ELF" H 3650 6134 50  0000 C CNN
F 2 "RESC3116X65N" H 3850 6050 50  0001 L CNN
F 3 "http://www.bourns.com/docs/Product-Datasheets/CMP-A.pdf" H 3850 5950 50  0001 L CNN
F 4 "Thick Film Resistors - SMD ResHighPowerA 1206 5k6 1% 3/4W TC100 AEC-Q200" H 3850 5850 50  0001 L CNN "Description"
F 5 "0.65" H 3850 5750 50  0001 L CNN "Height"
F 6 "Bourns" H 3850 5650 50  0001 L CNN "Manufacturer_Name"
F 7 "CMP1206AFX-5601ELF" H 3850 5550 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "652-CMP1206AFX-5601L" H 3850 5450 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Bourns/CMP1206AFX-5601ELF?qs=TiOZkKH1s2RUs9dCUW7AmQ%3D%3D" H 3850 5350 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 3850 5250 50  0001 L CNN "Arrow Part Number"
F 11 "" H 3850 5150 50  0001 L CNN "Arrow Price/Stock"
	1    3300 6000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 615FFCAC
P 4300 5450
F 0 "#PWR0111" H 4300 5200 50  0001 C CNN
F 1 "GND" H 4305 5277 50  0000 C CNN
F 2 "" H 4300 5450 50  0001 C CNN
F 3 "" H 4300 5450 50  0001 C CNN
	1    4300 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4200 5200 4300 5200
Wire Wire Line
	4300 5200 4300 5450
Wire Wire Line
	4000 6000 4400 6000
Connection ~ 4400 6000
Wire Wire Line
	3300 6000 3000 6000
Wire Wire Line
	3000 6000 3000 6250
Wire Wire Line
	3000 6000 3000 5300
Connection ~ 3000 6000
Text GLabel 1350 1200 1    50   Input ~ 0
10V
$Comp
L power:GND #PWR0112
U 1 1 6160B47D
P 3800 2300
F 0 "#PWR0112" H 3800 2050 50  0001 C CNN
F 1 "GND" H 3805 2127 50  0000 C CNN
F 2 "" H 3800 2300 50  0001 C CNN
F 3 "" H 3800 2300 50  0001 C CNN
	1    3800 2300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 6160C6B2
P 2100 2300
F 0 "#PWR0113" H 2100 2050 50  0001 C CNN
F 1 "GND" H 2105 2127 50  0000 C CNN
F 2 "" H 2100 2300 50  0001 C CNN
F 3 "" H 2100 2300 50  0001 C CNN
	1    2100 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 1950 2100 1950
Wire Wire Line
	2100 1950 2100 2300
Wire Wire Line
	2200 2050 2200 2200
Wire Wire Line
	2200 2200 3950 2200
Wire Wire Line
	3800 2050 3800 2300
Wire Wire Line
	1950 2600 2800 2600
Wire Wire Line
	1950 2700 3150 2700
Wire Wire Line
	2800 3500 2800 3650
Wire Wire Line
	2800 3650 3150 3650
Wire Wire Line
	3150 3650 3150 3500
Wire Wire Line
	1550 1200 1550 950 
Wire Wire Line
	1550 950  800  950 
Wire Wire Line
	800  950  800  3650
Wire Wire Line
	800  3650 2800 3650
Connection ~ 2800 3650
Wire Wire Line
	2800 2800 2800 2600
Wire Wire Line
	3150 2800 3150 2700
Wire Wire Line
	6850 850  6850 650 
$Comp
L SamacSys_Parts:240-004P J3
U 1 1 61653296
P 2050 3950
F 0 "J3" V 2296 4078 50  0000 L CNN
F 1 "240-004P" V 2387 4078 50  0000 L CNN
F 2 "HDRV6W64P0X254_1X6_1524X254X909P" H 2700 4050 50  0001 L CNN
F 3 "https://reference.digilentinc.com/_media/reference/instrumentation/6-pin-header-gender-changer/163-068-2.pdf" H 2700 3950 50  0001 L CNN
F 4 "Headers & Wire Housings 6 Pin Header and Gender Changer" H 2700 3850 50  0001 L CNN "Description"
F 5 "9.09" H 2700 3750 50  0001 L CNN "Height"
F 6 "DIGILENT" H 2700 3650 50  0001 L CNN "Manufacturer_Name"
F 7 "240-004P" H 2700 3550 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "424-6-PIN-HEADER" H 2700 3450 50  0001 L CNN "Mouser Part Number"
F 9 "http://www.mouser.com/Search/ProductDetail.aspx?qs=HqOR3aA30b7%2fBuZGjw%252bskg%3d%3d" H 2700 3350 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 2700 3250 50  0001 L CNN "Arrow Part Number"
F 11 "" H 2700 3150 50  0001 L CNN "Arrow Price/Stock"
	1    2050 3950
	0    1    1    0   
$EndComp
$Comp
L SamacSys_Parts:240-004P J2
U 1 1 6165D551
P 1550 5250
F 0 "J2" V 1888 4622 50  0000 R CNN
F 1 "240-004P" V 1797 4622 50  0000 R CNN
F 2 "HDRV6W64P0X254_1X6_1524X254X909P" H 2200 5350 50  0001 L CNN
F 3 "https://reference.digilentinc.com/_media/reference/instrumentation/6-pin-header-gender-changer/163-068-2.pdf" H 2200 5250 50  0001 L CNN
F 4 "Headers & Wire Housings 6 Pin Header and Gender Changer" H 2200 5150 50  0001 L CNN "Description"
F 5 "9.09" H 2200 5050 50  0001 L CNN "Height"
F 6 "DIGILENT" H 2200 4950 50  0001 L CNN "Manufacturer_Name"
F 7 "240-004P" H 2200 4850 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "424-6-PIN-HEADER" H 2200 4750 50  0001 L CNN "Mouser Part Number"
F 9 "http://www.mouser.com/Search/ProductDetail.aspx?qs=HqOR3aA30b7%2fBuZGjw%252bskg%3d%3d" H 2200 4650 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 2200 4550 50  0001 L CNN "Arrow Part Number"
F 11 "" H 2200 4450 50  0001 L CNN "Arrow Price/Stock"
	1    1550 5250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5500 3650 3500 3650
Wire Wire Line
	3500 3650 3500 3950
Wire Wire Line
	3500 3950 2050 3950
Connection ~ 2050 3950
Wire Wire Line
	2050 3950 1950 3950
Wire Wire Line
	1650 3950 1550 3950
Wire Wire Line
	700  3950 700  2700
Wire Wire Line
	700  2700 950  2700
Connection ~ 1550 3950
Wire Wire Line
	1550 3950 700  3950
Wire Wire Line
	2050 5250 1950 5250
Connection ~ 1650 5250
Wire Wire Line
	1650 5250 1550 5250
Connection ~ 1750 5250
Wire Wire Line
	1750 5250 1650 5250
Connection ~ 1850 5250
Wire Wire Line
	1850 5250 1750 5250
Connection ~ 1950 5250
Wire Wire Line
	1950 5250 1850 5250
Wire Wire Line
	2050 5250 2500 5250
Connection ~ 2050 5250
$Comp
L SamacSys_Parts:TEH140M50R0FE R5
U 1 1 6149C4C7
P 7850 700
F 0 "R5" H 8200 925 50  0000 C CNN
F 1 "TEH140M50R0FE" H 8200 834 50  0000 C CNN
F 2 "TEH140M50R0FE" H 8400 750 50  0001 L CNN
F 3 "https://www.mouser.com/datasheet/2/303/res_teh140-1488069.pdf" H 8400 650 50  0001 L CNN
F 4 "RES, 50R, 140W, TO-247, THICK FILM; Resistance:50ohm; Product Range:TEH140 Series; Power Rating:140W; Resistance Tolerance:+/- 1%; Resistor Case Style:TO-247; Voltage Rating:500V; Resistor Element Type:Thick Film RoHS Compliant: Yes" H 8400 550 50  0001 L CNN "Description"
F 5 "20.5" H 8400 450 50  0001 L CNN "Height"
F 6 "Ohmite" H 8400 350 50  0001 L CNN "Manufacturer_Name"
F 7 "TEH140M50R0FE" H 8400 250 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "588-TEH140M50R0FE" H 8400 150 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=588-TEH140M50R0FE" H 8400 50  50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 8400 -50 50  0001 L CNN "Arrow Part Number"
F 11 "" H 8400 -150 50  0001 L CNN "Arrow Price/Stock"
	1    7850 700 
	0    1    1    0   
$EndComp
Wire Wire Line
	6950 600  6950 850 
Wire Wire Line
	6950 600  7850 600 
Wire Wire Line
	8550 5100 9650 5100
Wire Wire Line
	9650 5100 9650 4900
Wire Wire Line
	8550 3850 8550 5100
Wire Wire Line
	7850 4800 9650 4800
Wire Wire Line
	7850 4800 7850 5800
Wire Wire Line
	1450 6850 1450 7000
Text GLabel 1550 6950 3    50   Input ~ 0
10V
Text GLabel 3100 5200 0    50   Input ~ 0
10V
Wire Wire Line
	4200 5100 4200 5000
Wire Wire Line
	4200 5000 2500 5000
Wire Wire Line
	2500 5000 2500 5250
$Comp
L SamacSys_Parts:SI5338C-B07590-GM IC4
U 1 1 615E4D94
P 4200 1950
F 0 "IC4" H 5444 1796 50  0000 L CNN
F 1 "SI5338C-B07590-GM" H 5444 1705 50  0000 L CNN
F 2 "QFN50P400X400X90-25N-D" H 5250 2550 50  0001 L CNN
F 3 "https://www.silabs.com/documents/public/data-sheets/Si5338.pdf" H 5250 2450 50  0001 L CNN
F 4 "Clock Generators & Support Products I2C control, 4-output, any frequency (< 200 MHz), any output, clock generator" H 5250 2350 50  0001 L CNN "Description"
F 5 "0.9" H 5250 2250 50  0001 L CNN "Height"
F 6 "Silicon Labs" H 5250 2150 50  0001 L CNN "Manufacturer_Name"
F 7 "SI5338C-B07590-GM" H 5250 2050 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 5250 1950 50  0001 L CNN "Mouser Part Number"
F 9 "" H 5250 1850 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 5250 1750 50  0001 L CNN "Arrow Part Number"
F 11 "" H 5250 1650 50  0001 L CNN "Arrow Price/Stock"
	1    4200 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 2050 4200 2050
Wire Wire Line
	3950 2050 3950 2200
Wire Wire Line
	3800 1950 4200 1950
Wire Wire Line
	2800 2600 2800 1050
Wire Wire Line
	2800 1050 5100 1050
Wire Wire Line
	5100 1050 5100 1150
Connection ~ 2800 2600
$Comp
L power:GND #PWR0114
U 1 1 6160A139
P 4500 800
F 0 "#PWR0114" H 4500 550 50  0001 C CNN
F 1 "GND" H 4505 627 50  0000 C CNN
F 2 "" H 4500 800 50  0001 C CNN
F 3 "" H 4500 800 50  0001 C CNN
	1    4500 800 
	-1   0    0    1   
$EndComp
Wire Wire Line
	3150 2700 4300 2700
Wire Wire Line
	4300 2700 4300 3250
Wire Wire Line
	4300 3250 5000 3250
Wire Wire Line
	5000 3250 5000 3150
Connection ~ 3150 2700
Wire Wire Line
	5400 1950 5500 1950
Wire Wire Line
	5500 1950 5500 3650
Wire Wire Line
	5600 950  5600 2150
Wire Wire Line
	5600 2150 5400 2150
Wire Wire Line
	4500 1150 4500 800 
Wire Wire Line
	1550 950  4050 950 
Connection ~ 1550 950 
Wire Wire Line
	4500 3150 4050 3150
Wire Wire Line
	4050 3150 4050 950 
Connection ~ 4050 950 
Wire Wire Line
	4050 950  5600 950 
$EndSCHEMATC
