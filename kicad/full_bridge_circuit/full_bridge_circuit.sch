EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Full Bridge Biasing Design"
Date "2021-09-21"
Rev "A"
Comp "Sivananthan Laboratories"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Module:Arduino_Nano_v3.x A1
U 1 1 614A3D40
P 1550 2950
F 0 "A1" H 1550 1861 50  0000 C CNN
F 1 "Arduino_Nano_v3.x" H 1550 1770 50  0000 C CNN
F 2 "Module:Arduino_Nano" H 1550 2950 50  0001 C CIN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 1550 2950 50  0001 C CNN
	1    1550 2950
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:FAN8811MNTXG IC5
U 1 1 614AF417
P 8000 4200
F 0 "IC5" H 8600 4465 50  0000 C CNN
F 1 "FAN8811MNTXG" H 8600 4374 50  0000 C CNN
F 2 "SON80P400X400X78-11N" H 9050 4300 50  0001 L CNN
F 3 "http://www.farnell.com/datasheets/3191468.pdf" H 9050 4200 50  0001 L CNN
F 4 "Gate Drivers HIGH/LOW SIDE GATE DRIVER" H 9050 4100 50  0001 L CNN "Description"
F 5 "0.78" H 9050 4000 50  0001 L CNN "Height"
F 6 "ON Semiconductor" H 9050 3900 50  0001 L CNN "Manufacturer_Name"
F 7 "FAN8811MNTXG" H 9050 3800 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "863-FAN8811MNTXG" H 9050 3700 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/FAN8811MNTXG?qs=HBWAp0VN4RhzEMpfwVWb7Q%3D%3D" H 9050 3600 50  0001 L CNN "Mouser Price/Stock"
F 10 "FAN8811MNTXG" H 9050 3500 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/fan8811mntxg/on-semiconductor?region=nac" H 9050 3400 50  0001 L CNN "Arrow Price/Stock"
	1    8000 4200
	-1   0    0    1   
$EndComp
$Comp
L SamacSys_Parts:FAN8811MNTXG IC4
U 1 1 614B34F4
P 8000 2200
F 0 "IC4" H 8600 2465 50  0000 C CNN
F 1 "FAN8811MNTXG" H 8600 2374 50  0000 C CNN
F 2 "SON80P400X400X78-11N" H 9050 2300 50  0001 L CNN
F 3 "http://www.farnell.com/datasheets/3191468.pdf" H 9050 2200 50  0001 L CNN
F 4 "Gate Drivers HIGH/LOW SIDE GATE DRIVER" H 9050 2100 50  0001 L CNN "Description"
F 5 "0.78" H 9050 2000 50  0001 L CNN "Height"
F 6 "ON Semiconductor" H 9050 1900 50  0001 L CNN "Manufacturer_Name"
F 7 "FAN8811MNTXG" H 9050 1800 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "863-FAN8811MNTXG" H 9050 1700 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/ON-Semiconductor/FAN8811MNTXG?qs=HBWAp0VN4RhzEMpfwVWb7Q%3D%3D" H 9050 1600 50  0001 L CNN "Mouser Price/Stock"
F 10 "FAN8811MNTXG" H 9050 1500 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/fan8811mntxg/on-semiconductor?region=nac" H 9050 1400 50  0001 L CNN "Arrow Price/Stock"
	1    8000 2200
	-1   0    0    1   
$EndComp
$Comp
L SamacSys_Parts:240-004P J1
U 1 1 614BB969
P 2450 6050
F 0 "J1" H 3078 5846 50  0000 L CNN
F 1 "240-004P" H 3078 5755 50  0000 L CNN
F 2 "HDRV6W64P0X254_1X6_1524X254X909P" H 3100 6150 50  0001 L CNN
F 3 "https://reference.digilentinc.com/_media/reference/instrumentation/6-pin-header-gender-changer/163-068-2.pdf" H 3100 6050 50  0001 L CNN
F 4 "Headers & Wire Housings 6 Pin Header and Gender Changer" H 3100 5950 50  0001 L CNN "Description"
F 5 "9.09" H 3100 5850 50  0001 L CNN "Height"
F 6 "DIGILENT" H 3100 5750 50  0001 L CNN "Manufacturer_Name"
F 7 "240-004P" H 3100 5650 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "424-6-PIN-HEADER" H 3100 5550 50  0001 L CNN "Mouser Part Number"
F 9 "http://www.mouser.com/Search/ProductDetail.aspx?qs=HqOR3aA30b7%2fBuZGjw%252bskg%3d%3d" H 3100 5450 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 3100 5350 50  0001 L CNN "Arrow Part Number"
F 11 "" H 3100 5250 50  0001 L CNN "Arrow Price/Stock"
	1    2450 6050
	-1   0    0    1   
$EndComp
$Comp
L SamacSys_Parts:240-004P J2
U 1 1 614BEE22
P 3250 7000
F 0 "J2" H 3542 6235 50  0000 C CNN
F 1 "240-004P" H 3542 6326 50  0000 C CNN
F 2 "HDRV6W64P0X254_1X6_1524X254X909P" H 3900 7100 50  0001 L CNN
F 3 "https://reference.digilentinc.com/_media/reference/instrumentation/6-pin-header-gender-changer/163-068-2.pdf" H 3900 7000 50  0001 L CNN
F 4 "Headers & Wire Housings 6 Pin Header and Gender Changer" H 3900 6900 50  0001 L CNN "Description"
F 5 "9.09" H 3900 6800 50  0001 L CNN "Height"
F 6 "DIGILENT" H 3900 6700 50  0001 L CNN "Manufacturer_Name"
F 7 "240-004P" H 3900 6600 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "424-6-PIN-HEADER" H 3900 6500 50  0001 L CNN "Mouser Part Number"
F 9 "http://www.mouser.com/Search/ProductDetail.aspx?qs=HqOR3aA30b7%2fBuZGjw%252bskg%3d%3d" H 3900 6400 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 3900 6300 50  0001 L CNN "Arrow Part Number"
F 11 "" H 3900 6200 50  0001 L CNN "Arrow Price/Stock"
	1    3250 7000
	-1   0    0    1   
$EndComp
$Comp
L SamacSys_Parts:284414-2 J3
U 1 1 614C645C
P 1500 650
F 0 "J3" V 1746 778 50  0000 L CNN
F 1 "284414-2" V 1837 778 50  0000 L CNN
F 2 "2844142" H 2150 750 50  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=284414&DocType=Customer+Drawing&DocLang=English" H 2150 650 50  0001 L CNN
F 4 "Fixed Terminal Blocks TERMI-BLOK 900 PCB 2 POS. 3,5" H 2150 550 50  0001 L CNN "Description"
F 5 "8.35" H 2150 450 50  0001 L CNN "Height"
F 6 "TE Connectivity" H 2150 350 50  0001 L CNN "Manufacturer_Name"
F 7 "284414-2" H 2150 250 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "571-284414-2" H 2150 150 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/TE-Connectivity/284414-2?qs=pW%2FyRk%2FT1EGY%252BVf9FdE6RA%3D%3D" H 2150 50  50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 2150 -50 50  0001 L CNN "Arrow Part Number"
F 11 "" H 2150 -150 50  0001 L CNN "Arrow Price/Stock"
	1    1500 650 
	0    1    1    0   
$EndComp
$Comp
L SamacSys_Parts:284414-2 J5
U 1 1 614CEB79
P 10650 800
F 0 "J5" V 10896 928 50  0000 L CNN
F 1 "284414-2" V 10987 928 50  0000 L CNN
F 2 "2844142" H 11300 900 50  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=284414&DocType=Customer+Drawing&DocLang=English" H 11300 800 50  0001 L CNN
F 4 "Fixed Terminal Blocks TERMI-BLOK 900 PCB 2 POS. 3,5" H 11300 700 50  0001 L CNN "Description"
F 5 "8.35" H 11300 600 50  0001 L CNN "Height"
F 6 "TE Connectivity" H 11300 500 50  0001 L CNN "Manufacturer_Name"
F 7 "284414-2" H 11300 400 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "571-284414-2" H 11300 300 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/TE-Connectivity/284414-2?qs=pW%2FyRk%2FT1EGY%252BVf9FdE6RA%3D%3D" H 11300 200 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 11300 100 50  0001 L CNN "Arrow Part Number"
F 11 "" H 11300 0   50  0001 L CNN "Arrow Price/Stock"
	1    10650 800 
	0    1    1    0   
$EndComp
$Comp
L SamacSys_Parts:284414-2 J4
U 1 1 614D95EC
P 10500 2950
F 0 "J4" V 10746 3078 50  0000 L CNN
F 1 "284414-2" V 10837 3078 50  0000 L CNN
F 2 "2844142" H 11150 3050 50  0001 L CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=284414&DocType=Customer+Drawing&DocLang=English" H 11150 2950 50  0001 L CNN
F 4 "Fixed Terminal Blocks TERMI-BLOK 900 PCB 2 POS. 3,5" H 11150 2850 50  0001 L CNN "Description"
F 5 "8.35" H 11150 2750 50  0001 L CNN "Height"
F 6 "TE Connectivity" H 11150 2650 50  0001 L CNN "Manufacturer_Name"
F 7 "284414-2" H 11150 2550 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "571-284414-2" H 11150 2450 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/TE-Connectivity/284414-2?qs=pW%2FyRk%2FT1EGY%252BVf9FdE6RA%3D%3D" H 11150 2350 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 11150 2250 50  0001 L CNN "Arrow Part Number"
F 11 "" H 11150 2150 50  0001 L CNN "Arrow Price/Stock"
	1    10500 2950
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:FA-238_25.0000MB-C3 Y1
U 1 1 614DC397
P 2600 2400
F 0 "Y1" H 3400 2665 50  0000 C CNN
F 1 "FA-238_25.0000MB-C3" H 3400 2574 50  0000 C CNN
F 2 "FA238300000MBC0" H 4050 2500 50  0001 L CNN
F 3 "https://support.epson.biz/td/api/doc_check.php?dl=brief_FA-238&lang=en" H 4050 2400 50  0001 L CNN
F 4 "Crystals 25MHz 50ppm 18pF -20C +70C" H 4050 2300 50  0001 L CNN "Description"
F 5 "0.7" H 4050 2200 50  0001 L CNN "Height"
F 6 "Epson Timing" H 4050 2100 50  0001 L CNN "Manufacturer_Name"
F 7 "FA-238 25.0000MB-C3" H 4050 2000 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "732-FA238-25B-C3" H 4050 1900 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Epson-Timing/FA-238-250000MB-C3?qs=CANAagUtPAqYIDF5A41Crg%3D%3D" H 4050 1800 50  0001 L CNN "Mouser Price/Stock"
F 10 "FA-238 25.0000MB-C3" H 4050 1700 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/fa-23825.0000mb-c3/epson-electronics-america" H 4050 1600 50  0001 L CNN "Arrow Price/Stock"
	1    2600 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 2500 2600 2700
Wire Wire Line
	2600 2700 4500 2700
$Comp
L power:GND #PWR0101
U 1 1 614F1BE2
P 4200 2500
F 0 "#PWR0101" H 4200 2250 50  0001 C CNN
F 1 "GND" H 4205 2327 50  0000 C CNN
F 2 "" H 4200 2500 50  0001 C CNN
F 3 "" H 4200 2500 50  0001 C CNN
	1    4200 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 614F3455
P 2500 2500
F 0 "#PWR0102" H 2500 2250 50  0001 C CNN
F 1 "GND" H 2505 2327 50  0000 C CNN
F 2 "" H 2500 2500 50  0001 C CNN
F 3 "" H 2500 2500 50  0001 C CNN
	1    2500 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 2400 2500 2400
Wire Wire Line
	2500 2400 2500 2500
$Comp
L SamacSys_Parts:CRCW04021K00FKEDC R1
U 1 1 614F6280
P 3000 3500
F 0 "R1" V 3304 3588 50  0000 L CNN
F 1 "CRCW04021K00FKEDC" V 3395 3588 50  0000 L CNN
F 2 "RESC1005X35N" H 3550 3550 50  0001 L CNN
F 3 "https://www.mouser.com/datasheet/2/427/crcwce3-1762584.pdf" H 3550 3450 50  0001 L CNN
F 4 "D10/CRCW0402-C 100 1K0 1% ET7" H 3550 3350 50  0001 L CNN "Description"
F 5 "0.35" H 3550 3250 50  0001 L CNN "Height"
F 6 "Vishay" H 3550 3150 50  0001 L CNN "Manufacturer_Name"
F 7 "CRCW04021K00FKEDC" H 3550 3050 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "71-CRCW04021K00FKEDC" H 3550 2950 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Vishay-Dale/CRCW04021K00FKEDC?qs=E3Y5ESvWgWMj5mY%252BhDIyqg%3D%3D" H 3550 2850 50  0001 L CNN "Mouser Price/Stock"
F 10 "CRCW04021K00FKEDC" H 3550 2750 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/crcw04021k00fkedc/vishay?region=nac" H 3550 2650 50  0001 L CNN "Arrow Price/Stock"
	1    3000 3500
	0    1    1    0   
$EndComp
$Comp
L SamacSys_Parts:CRCW04021K00FKEDC R2
U 1 1 614FA5B5
P 3200 4700
F 0 "R2" V 3596 4622 50  0000 R CNN
F 1 "CRCW04021K00FKEDC" V 3505 4622 50  0000 R CNN
F 2 "RESC1005X35N" H 3750 4750 50  0001 L CNN
F 3 "https://www.mouser.com/datasheet/2/427/crcwce3-1762584.pdf" H 3750 4650 50  0001 L CNN
F 4 "D10/CRCW0402-C 100 1K0 1% ET7" H 3750 4550 50  0001 L CNN "Description"
F 5 "0.35" H 3750 4450 50  0001 L CNN "Height"
F 6 "Vishay" H 3750 4350 50  0001 L CNN "Manufacturer_Name"
F 7 "CRCW04021K00FKEDC" H 3750 4250 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "71-CRCW04021K00FKEDC" H 3750 4150 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Vishay-Dale/CRCW04021K00FKEDC?qs=E3Y5ESvWgWMj5mY%252BhDIyqg%3D%3D" H 3750 4050 50  0001 L CNN "Mouser Price/Stock"
F 10 "CRCW04021K00FKEDC" H 3750 3950 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/crcw04021k00fkedc/vishay?region=nac" H 3750 3850 50  0001 L CNN "Arrow Price/Stock"
	1    3200 4700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2050 3450 3200 3450
Wire Wire Line
	3200 3450 3200 3400
Wire Wire Line
	3200 3450 3200 4000
Connection ~ 3200 3450
Wire Wire Line
	3000 3350 3000 3500
Connection ~ 3000 3350
Wire Wire Line
	3200 4700 3000 4700
Wire Wire Line
	3000 4700 3000 4200
Wire Wire Line
	3000 4700 900  4700
Wire Wire Line
	900  4700 900  1850
Wire Wire Line
	900  1850 1650 1850
Wire Wire Line
	1650 1850 1650 1950
Connection ~ 3000 4700
Text GLabel 1200 650  3    50   Input ~ 0
10V
$Comp
L power:GND #PWR0103
U 1 1 615063E6
P 1700 650
F 0 "#PWR0103" H 1700 400 50  0001 C CNN
F 1 "GND" H 1705 477 50  0000 C CNN
F 2 "" H 1700 650 50  0001 C CNN
F 3 "" H 1700 650 50  0001 C CNN
	1    1700 650 
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 650  1500 600 
Wire Wire Line
	1500 600  1700 600 
Wire Wire Line
	1700 600  1700 650 
Wire Wire Line
	1400 650  1400 600 
Wire Wire Line
	1400 600  1200 600 
Wire Wire Line
	1200 600  1200 650 
Wire Wire Line
	2450 5000 2450 5550
Connection ~ 2450 5550
Wire Wire Line
	2450 5550 2450 5650
Wire Wire Line
	2450 5950 2450 6050
Wire Wire Line
	2450 6050 750  6050
Wire Wire Line
	750  6050 750  3450
Wire Wire Line
	750  3450 1050 3450
Connection ~ 2450 6050
Text GLabel 1450 1750 1    50   Input ~ 0
10V
Wire Wire Line
	1450 1750 1450 1950
$Comp
L SamacSys_Parts:OPA810IDBVT IC1
U 1 1 61513773
P 4900 5750
F 0 "IC1" H 5450 5285 50  0000 C CNN
F 1 "OPA810IDBVT" H 5450 5376 50  0000 C CNN
F 2 "SOT95P280X145-5N" H 5850 5850 50  0001 L CNN
F 3 "https://www.ti.com/lit/ds/symlink/opa810.pdf?ts=1623319461078&ref_url=https%253A%252F%252Fwww.ti.com%252Fstore%252Fti%252Fen%252Fp%252Fproduct%252F%253Fp%253DOPA810IDBVT" H 5850 5750 50  0001 L CNN
F 4 "Operational Amplifiers - Op Amps Single Channel, High Performance, 27 V, 140 MHz, RRIO FET Input Op Amp 5-SOT-23 -40 to 125" H 5850 5650 50  0001 L CNN "Description"
F 5 "1.45" H 5850 5550 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 5850 5450 50  0001 L CNN "Manufacturer_Name"
F 7 "OPA810IDBVT" H 5850 5350 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "595-OPA810IDBVT" H 5850 5250 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Texas-Instruments/OPA810IDBVT?qs=xZ%2FP%252Ba9zWqaCn5bfRsQX9Q%3D%3D" H 5850 5150 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 5850 5050 50  0001 L CNN "Arrow Part Number"
F 11 "" H 5850 4950 50  0001 L CNN "Arrow Price/Stock"
	1    4900 5750
	-1   0    0    1   
$EndComp
$Comp
L SamacSys_Parts:CMP1206AFX-5601ELF R4
U 1 1 615193AE
P 4000 6250
F 0 "R4" H 4350 6475 50  0000 C CNN
F 1 "CMP1206AFX-5601ELF" H 4350 6384 50  0000 C CNN
F 2 "RESC3116X65N" H 4550 6300 50  0001 L CNN
F 3 "http://www.bourns.com/docs/Product-Datasheets/CMP-A.pdf" H 4550 6200 50  0001 L CNN
F 4 "Thick Film Resistors - SMD ResHighPowerA 1206 5k6 1% 3/4W TC100 AEC-Q200" H 4550 6100 50  0001 L CNN "Description"
F 5 "0.65" H 4550 6000 50  0001 L CNN "Height"
F 6 "Bourns" H 4550 5900 50  0001 L CNN "Manufacturer_Name"
F 7 "CMP1206AFX-5601ELF" H 4550 5800 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "652-CMP1206AFX-5601L" H 4550 5700 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Bourns/CMP1206AFX-5601ELF?qs=TiOZkKH1s2RUs9dCUW7AmQ%3D%3D" H 4550 5600 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 4550 5500 50  0001 L CNN "Arrow Part Number"
F 11 "" H 4550 5400 50  0001 L CNN "Arrow Price/Stock"
	1    4000 6250
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:RS73F1JTTD2001B R3
U 1 1 6151BA15
P 3800 6400
F 0 "R3" V 4104 6488 50  0000 L CNN
F 1 "RS73F1JTTD2001B" V 4195 6488 50  0000 L CNN
F 2 "RESC1608X55N" H 4350 6450 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/rs73f1jttd2001b/koa-speer-electronics" H 4350 6350 50  0001 L CNN
F 4 "Res Thick Film 0603 2K Ohm 0.1% 0.2W(1/5W) +/-25ppm/C Pad SMD Automotive T/R" H 4350 6250 50  0001 L CNN "Description"
F 5 "0.55" H 4350 6150 50  0001 L CNN "Height"
F 6 "KOA Speer" H 4350 6050 50  0001 L CNN "Manufacturer_Name"
F 7 "RS73F1JTTD2001B" H 4350 5950 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "660-RS73F1JTTD2001B" H 4350 5850 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/KOA-Speer/RS73F1JTTD2001B?qs=GBLSl2AkirvT9QL%252B%2FsjYag%3D%3D" H 4350 5750 50  0001 L CNN "Mouser Price/Stock"
F 10 "RS73F1JTTD2001B" H 4350 5650 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/rs73f1jttd2001b/koa-speer-electronics" H 4350 5550 50  0001 L CNN "Arrow Price/Stock"
	1    3800 6400
	0    1    1    0   
$EndComp
$Comp
L SamacSys_Parts:LM5134AMF_NOPB IC2
U 1 1 61526480
P 6650 5750
F 0 "IC2" H 7200 6015 50  0000 C CNN
F 1 "LM5134AMF_NOPB" H 7200 5924 50  0000 C CNN
F 2 "SOT95P280X145-6N" H 7600 5850 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/lm5134" H 7600 5750 50  0001 L CNN
F 4 "Single, 7.6A Peak Current Low-Side Gate Driver with Alternative Pilot Output" H 7600 5650 50  0001 L CNN "Description"
F 5 "1.45" H 7600 5550 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 7600 5450 50  0001 L CNN "Manufacturer_Name"
F 7 "LM5134AMF/NOPB" H 7600 5350 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 7600 5250 50  0001 L CNN "Mouser Part Number"
F 9 "" H 7600 5150 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 7600 5050 50  0001 L CNN "Arrow Part Number"
F 11 "" H 7600 4950 50  0001 L CNN "Arrow Price/Stock"
	1    6650 5750
	-1   0    0    1   
$EndComp
$Comp
L SamacSys_Parts:LM5134AMF_NOPB IC3
U 1 1 6152854E
P 6650 6750
F 0 "IC3" H 7200 7015 50  0000 C CNN
F 1 "LM5134AMF_NOPB" H 7200 6924 50  0000 C CNN
F 2 "SOT95P280X145-6N" H 7600 6850 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/lm5134" H 7600 6750 50  0001 L CNN
F 4 "Single, 7.6A Peak Current Low-Side Gate Driver with Alternative Pilot Output" H 7600 6650 50  0001 L CNN "Description"
F 5 "1.45" H 7600 6550 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 7600 6450 50  0001 L CNN "Manufacturer_Name"
F 7 "LM5134AMF/NOPB" H 7600 6350 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 7600 6250 50  0001 L CNN "Mouser Part Number"
F 9 "" H 7600 6150 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 7600 6050 50  0001 L CNN "Arrow Part Number"
F 11 "" H 7600 5950 50  0001 L CNN "Arrow Price/Stock"
	1    6650 6750
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 6153C9A6
P 3800 7250
F 0 "#PWR0104" H 3800 7000 50  0001 C CNN
F 1 "GND" H 3805 7077 50  0000 C CNN
F 2 "" H 3800 7250 50  0001 C CNN
F 3 "" H 3800 7250 50  0001 C CNN
	1    3800 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 5750 4900 6250
Wire Wire Line
	4900 6250 4700 6250
Wire Wire Line
	4000 6250 3800 6250
Wire Wire Line
	3800 6250 3800 6400
Wire Wire Line
	3800 7100 3800 7250
Wire Wire Line
	3800 6250 3800 5750
Connection ~ 3800 6250
Wire Wire Line
	3450 6750 3250 6750
Wire Wire Line
	3250 6750 3250 6700
Wire Wire Line
	3250 6600 3250 6700
Connection ~ 3250 6700
Wire Wire Line
	3250 6500 3250 6600
Connection ~ 3250 6600
Wire Wire Line
	3250 6750 3250 6800
Connection ~ 3250 6750
Wire Wire Line
	3250 6800 3250 6900
Connection ~ 3250 6800
Wire Wire Line
	3250 7000 3250 6900
Connection ~ 3250 6900
Wire Wire Line
	4900 5550 4900 5200
Wire Wire Line
	4900 5200 3450 5200
Wire Wire Line
	3450 5200 3450 6750
Text GLabel 3800 5450 1    50   Input ~ 0
10V
Wire Wire Line
	3800 5450 3800 5650
Wire Wire Line
	4900 5750 5400 5750
Wire Wire Line
	5400 5750 5400 5650
Wire Wire Line
	5400 5650 5550 5650
Connection ~ 4900 5750
$Comp
L power:GND #PWR0105
U 1 1 61562E79
P 5300 5850
F 0 "#PWR0105" H 5300 5600 50  0001 C CNN
F 1 "GND" H 5305 5677 50  0000 C CNN
F 2 "" H 5300 5850 50  0001 C CNN
F 3 "" H 5300 5850 50  0001 C CNN
	1    5300 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 5650 5300 5650
Wire Wire Line
	5300 5650 5300 5850
Wire Wire Line
	5550 5550 5300 5550
Wire Wire Line
	5300 5550 5300 5650
Connection ~ 5300 5650
Wire Wire Line
	5550 5750 5550 6050
Wire Wire Line
	5550 6050 6650 6050
Wire Wire Line
	6650 6050 6650 5750
Text GLabel 6100 6050 3    50   Input ~ 0
10V
$Comp
L power:GND #PWR0106
U 1 1 61567C53
P 5300 6900
F 0 "#PWR0106" H 5300 6650 50  0001 C CNN
F 1 "GND" H 5305 6727 50  0000 C CNN
F 2 "" H 5300 6900 50  0001 C CNN
F 3 "" H 5300 6900 50  0001 C CNN
	1    5300 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 6550 5300 6550
Wire Wire Line
	5300 6550 5300 6750
Wire Wire Line
	5550 6750 5300 6750
Connection ~ 5300 6750
Wire Wire Line
	5300 6750 5300 6900
Wire Wire Line
	4900 6250 4900 6650
Wire Wire Line
	4900 6650 5550 6650
Connection ~ 4900 6250
Text GLabel 6650 7050 3    50   Input ~ 0
10V
Text GLabel 6800 6550 2    50   Input ~ 0
LI
Text GLabel 6800 5550 2    50   Input ~ 0
HI
Wire Wire Line
	6650 6750 6650 7050
Wire Wire Line
	6650 6550 6800 6550
Wire Wire Line
	6650 5550 6800 5550
Text GLabel 6600 3900 0    50   Input ~ 0
HI
Text GLabel 6600 4000 0    50   Input ~ 0
LI
Text GLabel 6650 1900 0    50   Input ~ 0
LI
Text GLabel 6650 2000 0    50   Input ~ 0
HI
$Comp
L power:GND #PWR0107
U 1 1 61573047
P 6650 2350
F 0 "#PWR0107" H 6650 2100 50  0001 C CNN
F 1 "GND" H 6655 2177 50  0000 C CNN
F 2 "" H 6650 2350 50  0001 C CNN
F 3 "" H 6650 2350 50  0001 C CNN
	1    6650 2350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 61574690
P 6600 4350
F 0 "#PWR0108" H 6600 4100 50  0001 C CNN
F 1 "GND" H 6605 4177 50  0000 C CNN
F 2 "" H 6600 4350 50  0001 C CNN
F 3 "" H 6600 4350 50  0001 C CNN
	1    6600 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 61576E61
P 7550 3300
F 0 "#PWR0109" H 7550 3050 50  0001 C CNN
F 1 "GND" H 7555 3127 50  0000 C CNN
F 2 "" H 7550 3300 50  0001 C CNN
F 3 "" H 7550 3300 50  0001 C CNN
	1    7550 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 6157742D
P 7550 1300
F 0 "#PWR0110" H 7550 1050 50  0001 C CNN
F 1 "GND" H 7555 1127 50  0000 C CNN
F 2 "" H 7550 1300 50  0001 C CNN
F 3 "" H 7550 1300 50  0001 C CNN
	1    7550 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 1300 7400 1300
Wire Wire Line
	7550 3300 7400 3300
Wire Wire Line
	6600 3900 6800 3900
Wire Wire Line
	6600 4000 6800 4000
Wire Wire Line
	6800 4100 6600 4100
Wire Wire Line
	6600 4100 6600 4350
Wire Wire Line
	6800 2100 6650 2100
Wire Wire Line
	6650 2100 6650 2350
Wire Wire Line
	6800 2000 6650 2000
Wire Wire Line
	6800 1900 6650 1900
$Comp
L SamacSys_Parts:CHP0805AJW-100ELF R5
U 1 1 6159210E
P 8650 1600
F 0 "R5" H 9000 1825 50  0000 C CNN
F 1 "CHP0805AJW-100ELF" H 9000 1734 50  0000 C CNN
F 2 "RESC2012X60N" H 9200 1650 50  0001 L CNN
F 3 "https://www.mouser.com/datasheet/2/54/chp-a-1858677.pdf" H 9200 1550 50  0001 L CNN
F 4 "Thick Film Resistors - SMD ResHighPowerA 0805 10R 5% 1/2W TC200 AEC-Q200" H 9200 1450 50  0001 L CNN "Description"
F 5 "0.6" H 9200 1350 50  0001 L CNN "Height"
F 6 "Bourns" H 9200 1250 50  0001 L CNN "Manufacturer_Name"
F 7 "CHP0805AJW-100ELF" H 9200 1150 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "652-CHP0805AJW-100L" H 9200 1050 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Bourns/CHP0805AJW-100ELF?qs=GedFDFLaBXFSSMiH%252B2UbRQ%3D%3D" H 9200 950 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 9200 850 50  0001 L CNN "Arrow Part Number"
F 11 "" H 9200 750 50  0001 L CNN "Arrow Price/Stock"
	1    8650 1600
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:CHP0805AJW-100ELF R6
U 1 1 61594F92
P 8700 3650
F 0 "R6" H 9050 3875 50  0000 C CNN
F 1 "CHP0805AJW-100ELF" H 9050 3784 50  0000 C CNN
F 2 "RESC2012X60N" H 9250 3700 50  0001 L CNN
F 3 "https://www.mouser.com/datasheet/2/54/chp-a-1858677.pdf" H 9250 3600 50  0001 L CNN
F 4 "Thick Film Resistors - SMD ResHighPowerA 0805 10R 5% 1/2W TC200 AEC-Q200" H 9250 3500 50  0001 L CNN "Description"
F 5 "0.6" H 9250 3400 50  0001 L CNN "Height"
F 6 "Bourns" H 9250 3300 50  0001 L CNN "Manufacturer_Name"
F 7 "CHP0805AJW-100ELF" H 9250 3200 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "652-CHP0805AJW-100L" H 9250 3100 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Bourns/CHP0805AJW-100ELF?qs=GedFDFLaBXFSSMiH%252B2UbRQ%3D%3D" H 9250 3000 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 9250 2900 50  0001 L CNN "Arrow Part Number"
F 11 "" H 9250 2800 50  0001 L CNN "Arrow Price/Stock"
	1    8700 3650
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:CHP0805AJW-100ELF R7
U 1 1 6159586D
P 8700 4450
F 0 "R7" H 9050 4675 50  0000 C CNN
F 1 "CHP0805AJW-100ELF" H 9050 4584 50  0000 C CNN
F 2 "RESC2012X60N" H 9250 4500 50  0001 L CNN
F 3 "https://www.mouser.com/datasheet/2/54/chp-a-1858677.pdf" H 9250 4400 50  0001 L CNN
F 4 "Thick Film Resistors - SMD ResHighPowerA 0805 10R 5% 1/2W TC200 AEC-Q200" H 9250 4300 50  0001 L CNN "Description"
F 5 "0.6" H 9250 4200 50  0001 L CNN "Height"
F 6 "Bourns" H 9250 4100 50  0001 L CNN "Manufacturer_Name"
F 7 "CHP0805AJW-100ELF" H 9250 4000 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "652-CHP0805AJW-100L" H 9250 3900 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Bourns/CHP0805AJW-100ELF?qs=GedFDFLaBXFSSMiH%252B2UbRQ%3D%3D" H 9250 3800 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 9250 3700 50  0001 L CNN "Arrow Part Number"
F 11 "" H 9250 3600 50  0001 L CNN "Arrow Price/Stock"
	1    8700 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 615A05E4
P 10350 850
F 0 "#PWR0111" H 10350 600 50  0001 C CNN
F 1 "GND" H 10355 677 50  0000 C CNN
F 2 "" H 10350 850 50  0001 C CNN
F 3 "" H 10350 850 50  0001 C CNN
	1    10350 850 
	1    0    0    -1  
$EndComp
Text GLabel 10650 700  1    50   Input ~ 0
VDC
Wire Wire Line
	10650 800  10650 700 
Wire Wire Line
	10550 800  10350 800 
Wire Wire Line
	10350 800  10350 850 
Text GLabel 9750 1200 1    50   Input ~ 0
VDC
Text GLabel 9750 3250 1    50   Input ~ 0
VDC
Wire Wire Line
	9450 4450 9400 4450
Wire Wire Line
	9450 3650 9400 3650
$Comp
L SamacSys_Parts:IPP65R060CFD7XKSA1 IC8
U 1 1 615B1560
P 9450 3650
F 0 "IC8" H 9960 3796 50  0000 L CNN
F 1 "IPP65R060CFD7XKSA1" H 9960 3705 50  0000 L CNN
F 2 "TO254P457X1003X2115-3P" H 10030 3700 50  0001 L CNN
F 3 "https://www.infineon.com/dgdl/Infineon-IPP65R060CFD7-DataSheet-v02_01-EN.pdf?fileId=5546d46272e49d2a01730951be6b44f1" H 10030 3600 50  0001 L CNN
F 4 "650V CoolMOS CFD7 SJ PowerDevice" H 10030 3500 50  0001 L CNN "Description"
F 5 "4.57" H 10030 3400 50  0001 L CNN "Height"
F 6 "Infineon" H 10030 3300 50  0001 L CNN "Manufacturer_Name"
F 7 "IPP65R060CFD7XKSA1" H 10030 3200 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "726-IPP65R060CFD7SA1" H 10030 3100 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Infineon-Technologies/IPP65R060CFD7XKSA1/?qs=DPoM0jnrROWNS4HGGZ8UzA%3D%3D" H 10030 3000 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 10030 2900 50  0001 L CNN "Arrow Part Number"
F 11 "" H 10030 2800 50  0001 L CNN "Arrow Price/Stock"
	1    9450 3650
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:IPP65R060CFD7XKSA1 IC6
U 1 1 615BA720
P 9450 1600
F 0 "IC6" H 9960 1746 50  0000 L CNN
F 1 "IPP65R060CFD7XKSA1" H 9960 1655 50  0000 L CNN
F 2 "TO254P457X1003X2115-3P" H 10030 1650 50  0001 L CNN
F 3 "https://www.infineon.com/dgdl/Infineon-IPP65R060CFD7-DataSheet-v02_01-EN.pdf?fileId=5546d46272e49d2a01730951be6b44f1" H 10030 1550 50  0001 L CNN
F 4 "650V CoolMOS CFD7 SJ PowerDevice" H 10030 1450 50  0001 L CNN "Description"
F 5 "4.57" H 10030 1350 50  0001 L CNN "Height"
F 6 "Infineon" H 10030 1250 50  0001 L CNN "Manufacturer_Name"
F 7 "IPP65R060CFD7XKSA1" H 10030 1150 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "726-IPP65R060CFD7SA1" H 10030 1050 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Infineon-Technologies/IPP65R060CFD7XKSA1/?qs=DPoM0jnrROWNS4HGGZ8UzA%3D%3D" H 10030 950 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 10030 850 50  0001 L CNN "Arrow Part Number"
F 11 "" H 10030 750 50  0001 L CNN "Arrow Price/Stock"
	1    9450 1600
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:IPP65R060CFD7XKSA1 IC7
U 1 1 615BC43E
P 9450 2400
F 0 "IC7" H 9960 2546 50  0000 L CNN
F 1 "IPP65R060CFD7XKSA1" H 9960 2455 50  0000 L CNN
F 2 "TO254P457X1003X2115-3P" H 10030 2450 50  0001 L CNN
F 3 "https://www.infineon.com/dgdl/Infineon-IPP65R060CFD7-DataSheet-v02_01-EN.pdf?fileId=5546d46272e49d2a01730951be6b44f1" H 10030 2350 50  0001 L CNN
F 4 "650V CoolMOS CFD7 SJ PowerDevice" H 10030 2250 50  0001 L CNN "Description"
F 5 "4.57" H 10030 2150 50  0001 L CNN "Height"
F 6 "Infineon" H 10030 2050 50  0001 L CNN "Manufacturer_Name"
F 7 "IPP65R060CFD7XKSA1" H 10030 1950 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "726-IPP65R060CFD7SA1" H 10030 1850 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Infineon-Technologies/IPP65R060CFD7XKSA1/?qs=DPoM0jnrROWNS4HGGZ8UzA%3D%3D" H 10030 1750 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 10030 1650 50  0001 L CNN "Arrow Part Number"
F 11 "" H 10030 1550 50  0001 L CNN "Arrow Price/Stock"
	1    9450 2400
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:IPP65R060CFD7XKSA1 IC9
U 1 1 615BDD81
P 9450 4450
F 0 "IC9" H 9960 4596 50  0000 L CNN
F 1 "IPP65R060CFD7XKSA1" H 9960 4505 50  0000 L CNN
F 2 "TO254P457X1003X2115-3P" H 10030 4500 50  0001 L CNN
F 3 "https://www.infineon.com/dgdl/Infineon-IPP65R060CFD7-DataSheet-v02_01-EN.pdf?fileId=5546d46272e49d2a01730951be6b44f1" H 10030 4400 50  0001 L CNN
F 4 "650V CoolMOS CFD7 SJ PowerDevice" H 10030 4300 50  0001 L CNN "Description"
F 5 "4.57" H 10030 4200 50  0001 L CNN "Height"
F 6 "Infineon" H 10030 4100 50  0001 L CNN "Manufacturer_Name"
F 7 "IPP65R060CFD7XKSA1" H 10030 4000 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "726-IPP65R060CFD7SA1" H 10030 3900 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Infineon-Technologies/IPP65R060CFD7XKSA1/?qs=DPoM0jnrROWNS4HGGZ8UzA%3D%3D" H 10030 3800 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 10030 3700 50  0001 L CNN "Arrow Part Number"
F 11 "" H 10030 3600 50  0001 L CNN "Arrow Price/Stock"
	1    9450 4450
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:EEV-FK1K680Q C1
U 1 1 615FE35E
P 8550 3000
F 0 "C1" H 8800 3267 50  0000 C CNN
F 1 "EEV-FK1K680Q" H 8800 3176 50  0000 C CNN
F 2 "CAPAE1350X1400N" H 8900 3050 50  0001 L CNN
F 3 "http://www.farnell.com/datasheets/2813657.pdf" H 8900 2950 50  0001 L CNN
F 4 "Panasonic 68F 80V dc Aluminium Electrolytic Capacitor, Surface Mount 12.5 (Dia.) x 13.5mm +105C 12.5mm" H 8900 2850 50  0001 L CNN "Description"
F 5 "14" H 8900 2750 50  0001 L CNN "Height"
F 6 "Panasonic" H 8900 2650 50  0001 L CNN "Manufacturer_Name"
F 7 "EEV-FK1K680Q" H 8900 2550 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "667-EEV-FK1K680Q" H 8900 2450 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Panasonic/EEV-FK1K680Q?qs=RxiRFHJBgLw0LWVQGQIUtg%3D%3D" H 8900 2350 50  0001 L CNN "Mouser Price/Stock"
F 10 "EEV-FK1K680Q" H 8900 2250 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/eev-fk1k680q/panasonic?region=nac" H 8900 2150 50  0001 L CNN "Arrow Price/Stock"
	1    8550 3000
	-1   0    0    1   
$EndComp
$Comp
L SamacSys_Parts:EEV-FK1K680Q C2
U 1 1 61600F8E
P 8550 5000
F 0 "C2" H 8800 5267 50  0000 C CNN
F 1 "EEV-FK1K680Q" H 8800 5176 50  0000 C CNN
F 2 "CAPAE1350X1400N" H 8900 5050 50  0001 L CNN
F 3 "http://www.farnell.com/datasheets/2813657.pdf" H 8900 4950 50  0001 L CNN
F 4 "Panasonic 68F 80V dc Aluminium Electrolytic Capacitor, Surface Mount 12.5 (Dia.) x 13.5mm +105C 12.5mm" H 8900 4850 50  0001 L CNN "Description"
F 5 "14" H 8900 4750 50  0001 L CNN "Height"
F 6 "Panasonic" H 8900 4650 50  0001 L CNN "Manufacturer_Name"
F 7 "EEV-FK1K680Q" H 8900 4550 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "667-EEV-FK1K680Q" H 8900 4450 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Panasonic/EEV-FK1K680Q?qs=RxiRFHJBgLw0LWVQGQIUtg%3D%3D" H 8900 4350 50  0001 L CNN "Mouser Price/Stock"
F 10 "EEV-FK1K680Q" H 8900 4250 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/eev-fk1k680q/panasonic?region=nac" H 8900 4150 50  0001 L CNN "Arrow Price/Stock"
	1    8550 5000
	-1   0    0    1   
$EndComp
Wire Wire Line
	6800 2200 6800 2400
Wire Wire Line
	6800 2400 8650 2400
Wire Wire Line
	9450 2400 9350 2400
Wire Wire Line
	9450 1600 9350 1600
Wire Wire Line
	8650 1600 8650 2000
Wire Wire Line
	8650 2000 8000 2000
Wire Wire Line
	9750 1900 9750 1800
Wire Wire Line
	9750 2000 9750 1900
Connection ~ 9750 1900
Wire Wire Line
	8550 2100 8550 3000
Text GLabel 8000 2550 0    50   Input ~ 0
10V
Wire Wire Line
	8000 1900 8050 1900
Wire Wire Line
	8000 2100 8550 2100
Wire Wire Line
	8050 3000 8050 1900
Connection ~ 8050 1900
Wire Wire Line
	8050 1900 9750 1900
Wire Wire Line
	8000 2200 8000 2550
Wire Wire Line
	9750 3850 9750 3900
Wire Wire Line
	8000 4100 8550 4100
Wire Wire Line
	8550 4100 8550 5000
Wire Wire Line
	8000 3900 8050 3900
Connection ~ 9750 3900
Wire Wire Line
	9750 3900 9750 4050
Wire Wire Line
	8050 5000 8050 3900
Connection ~ 8050 3900
Wire Wire Line
	8050 3900 9750 3900
Wire Wire Line
	8700 4000 8700 3650
Wire Wire Line
	8000 4000 8700 4000
Wire Wire Line
	6800 4200 6800 4450
Wire Wire Line
	6800 4450 8700 4450
$Comp
L power:GND #PWR0112
U 1 1 6165DBA0
P 9750 2600
F 0 "#PWR0112" H 9750 2350 50  0001 C CNN
F 1 "GND" H 9755 2427 50  0000 C CNN
F 2 "" H 9750 2600 50  0001 C CNN
F 3 "" H 9750 2600 50  0001 C CNN
	1    9750 2600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 6165E00E
P 9750 4650
F 0 "#PWR0113" H 9750 4400 50  0001 C CNN
F 1 "GND" H 9755 4477 50  0000 C CNN
F 2 "" H 9750 4650 50  0001 C CNN
F 3 "" H 9750 4650 50  0001 C CNN
	1    9750 4650
	1    0    0    -1  
$EndComp
Text GLabel 8000 4600 0    50   Input ~ 0
10V
Wire Wire Line
	8000 4200 8000 4600
Wire Wire Line
	9750 1900 10500 1900
Wire Wire Line
	10500 1900 10500 2950
Wire Wire Line
	9750 3900 10500 3900
Wire Wire Line
	10500 3900 10500 3050
$Comp
L power:GND #PWR0114
U 1 1 614BB9D4
P 1850 3950
F 0 "#PWR0114" H 1850 3700 50  0001 C CNN
F 1 "GND" H 1855 3777 50  0000 C CNN
F 2 "" H 1850 3950 50  0001 C CNN
F 3 "" H 1850 3950 50  0001 C CNN
	1    1850 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 3950 1650 3950
Connection ~ 1650 3950
Wire Wire Line
	1650 3950 1550 3950
Wire Wire Line
	2050 3350 3000 3350
$Comp
L SamacSys_Parts:CHP0805AJW-100ELF R8
U 1 1 614E39CB
P 8650 2400
F 0 "R8" H 9000 2625 50  0000 C CNN
F 1 "CHP0805AJW-100ELF" H 9000 2534 50  0000 C CNN
F 2 "RESC2012X60N" H 9200 2450 50  0001 L CNN
F 3 "https://www.mouser.com/datasheet/2/54/chp-a-1858677.pdf" H 9200 2350 50  0001 L CNN
F 4 "Thick Film Resistors - SMD ResHighPowerA 0805 10R 5% 1/2W TC200 AEC-Q200" H 9200 2250 50  0001 L CNN "Description"
F 5 "0.6" H 9200 2150 50  0001 L CNN "Height"
F 6 "Bourns" H 9200 2050 50  0001 L CNN "Manufacturer_Name"
F 7 "CHP0805AJW-100ELF" H 9200 1950 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "652-CHP0805AJW-100L" H 9200 1850 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Bourns/CHP0805AJW-100ELF?qs=GedFDFLaBXFSSMiH%252B2UbRQ%3D%3D" H 9200 1750 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 9200 1650 50  0001 L CNN "Arrow Part Number"
F 11 "" H 9200 1550 50  0001 L CNN "Arrow Price/Stock"
	1    8650 2400
	1    0    0    -1  
$EndComp
Text GLabel 8650 1700 0    50   Input ~ 0
HO
Text GLabel 8300 1900 1    50   Input ~ 0
HS
Text GLabel 8550 2100 2    50   Input ~ 0
HB
Text GLabel 4900 6100 2    50   Input ~ 0
VO
$Comp
L SamacSys_Parts:SI5338C-B07590-GM IC?
U 1 1 614DBE6A
P 4600 2400
F 0 "IC?" H 5844 2246 50  0000 L CNN
F 1 "SI5338C-B07590-GM" H 5844 2155 50  0000 L CNN
F 2 "QFN50P400X400X90-25N-D" H 5650 3000 50  0001 L CNN
F 3 "https://www.silabs.com/documents/public/data-sheets/Si5338.pdf" H 5650 2900 50  0001 L CNN
F 4 "Clock Generators & Support Products I2C control, 4-output, any frequency (< 200 MHz), any output, clock generator" H 5650 2800 50  0001 L CNN "Description"
F 5 "0.9" H 5650 2700 50  0001 L CNN "Height"
F 6 "Silicon Labs" H 5650 2600 50  0001 L CNN "Manufacturer_Name"
F 7 "SI5338C-B07590-GM" H 5650 2500 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 5650 2400 50  0001 L CNN "Mouser Part Number"
F 9 "" H 5650 2300 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 5650 2200 50  0001 L CNN "Arrow Part Number"
F 11 "" H 5650 2100 50  0001 L CNN "Arrow Price/Stock"
	1    4600 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 2500 4600 2500
Wire Wire Line
	4500 2500 4500 2700
Wire Wire Line
	4200 2400 4600 2400
Wire Wire Line
	5500 1500 5500 1600
Wire Wire Line
	3000 1500 3000 3350
Wire Wire Line
	4400 3600 4750 3600
Wire Wire Line
	3200 3400 4300 3400
Wire Wire Line
	4300 3400 4300 3700
Wire Wire Line
	4300 3700 5400 3700
Wire Wire Line
	5400 3700 5400 3600
Wire Wire Line
	3000 1500 5500 1500
Wire Wire Line
	1650 1850 1650 1300
Wire Wire Line
	1650 1300 4400 1300
Wire Wire Line
	4400 1300 4400 3600
Connection ~ 1650 1850
Wire Wire Line
	4750 3600 4750 3850
Wire Wire Line
	4750 3850 5900 3850
Wire Wire Line
	5900 3850 5900 2600
Wire Wire Line
	5900 2600 5800 2600
Connection ~ 4750 3600
Wire Wire Line
	4750 3600 4900 3600
Wire Wire Line
	5800 2400 6050 2400
Wire Wire Line
	6050 2400 6050 5000
Wire Wire Line
	2450 5000 6050 5000
$Comp
L power:GND #PWR?
U 1 1 61590D36
P 4900 1250
F 0 "#PWR?" H 4900 1000 50  0001 C CNN
F 1 "GND" H 4905 1077 50  0000 C CNN
F 2 "" H 4900 1250 50  0001 C CNN
F 3 "" H 4900 1250 50  0001 C CNN
	1    4900 1250
	-1   0    0    1   
$EndComp
Wire Wire Line
	4900 1250 4900 1350
Wire Wire Line
	4900 1350 5100 1350
Wire Wire Line
	5100 1350 5100 1600
Connection ~ 4900 1350
Wire Wire Line
	4900 1350 4900 1600
$EndSCHEMATC
