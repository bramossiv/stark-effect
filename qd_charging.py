#%% modules
import numpy as np
import pandas as pd
import altair as alt

alt.renderers.enable('mimetype')
#%% setup data
"""
quantum dot biasing pads are long interlocking fingers
    - 1mm x 1mm cell
    - 10um distance
    - unknown biasing pad depth

Capacitance formula:
    C = E_0 (A / d)
"""
N = 100
length = 23 * (100 // 4) * 1e-6
distance = 100e-6
dielectrics = np.array([1, 10])#, 100])
# depths = np.linspace(0.1, 5.1, N) * 1e-6
depths = np.linspace(0.1, 100.1, N) * 1e-9

data = {'E_0':[], 'depth':[], 'cap':[]}
for e in dielectrics:
    caps = e * depths * length / distance
    data['E_0'] += [e] * N
    data['depth'] += depths.tolist()
    data['cap'] += caps.tolist()

# %% plot
df = pd.DataFrame(data=data)
df['depth'] *= 1e9
df['cap'] *= 1e6

x = alt.X('depth',
    title='Depth (nm)')
y = alt.Y('cap', 
    title='Capacitance (uF)',
    scale=alt.Scale(type='log'))
color = alt.Color('E_0:N', legend=alt.Legend(title='Dielectric Constant'))

lines = alt.Chart(df).mark_line().encode(
    x = x,
    y = y,
    color = color
)
lines
# %%
